// --------------Navigation-----------------
export const additionalResourcesScreenNavigation = {
  SUBJECT_ADDITIONAL_RESOURCES_SCREEN: "subjectAdditionalResourcesScreen",
  CHAPTER_ADDITIONAL_RESOURCES_SCREEN: "chapterAdditionalResourcesScreen",
  TOPIC_ADDITIONAL_RESOURCES_SCREEN: "topicAdditionalResourcesScreen",
  PDF_VIEWER_SCREEN: "chapterAdditionalResourcesScreen",
  AUDIO_SCREEN: "audioDialog",
  VIDEO_SCREEN: "videoScreen",
};
// --------------Action Area-----------------
export const additionalResourcesActionArea = {
  SUBJECT_CLICKED: "subjectClicked",
  CHAPTER_CLICKED: "chapterClicked",
  READ_BUTTON_CLICKED: "readButtonClicked",
  AUDIO_BUTTON_CLICKED: "audioButtonClicked",
  VIDEO_BUTTON_CLICKED: "videoButtonClicked",
};
// --------------Attributes-----------------
export const additionalResourcesAttributes = {
  SUBJECT_ID: "subjectId",
  CHAPTER_ID: "chapterId",
  TOPIC_ID: "topicId",
  PDF_URL: "pdfURL",
  AUDIO_URL: "audioURL",
  VIDEO_URL: "videoURL",
};
