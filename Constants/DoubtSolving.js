// --------------Navigation-----------------
export const doubtSolvingNavigation = {
  RESOVLED: "resolvedScreen",
  OPEN: "openScreen",
  FLAGGED: "flaggedScreen",
  SOLUTION_MODAL: "solutionModalScreen",
  ASK_DOUBT_MODAL: "askDoubtModalScreen",
  DELETE_DOUBT_MODAL: "deleteDoubtModalScreen",
};
// --------------Action Area-----------------
export const doubtSolvingUserActionArea = {
  DOUBT_TYPE_TAB: "doubtTypeTab",
  SUBJECT_DROPDOWN: "subjectDropdown",
  VIEW_SOLUTION: "viewSolution",
  EDIT_DOUBT: "editDoubt",
  ASK_DOUBT_CARD: "askDoubtCard",
  DELETE_DOUBT: "deleteDoubt",
  CONFIRM_DELETE_DOUBT: "confirmDeleteDoubt",
  CANCEL_DELETE_DOUBT: "cancelDeleteDoubt",
  SUBJECT_BUTTON: "subjectButton",
  ADD_IMAGES: "addImages",
  ASK_DOUBT: "askDoubt",
  CLOSE_DOUBT: "closeDoubt",
};
// --------------Attributes-----------------
export const doubtSolvingAttributes = {
  SUBJECT_ID: "subjectId",
  DOUBT_ID: "doubtId",
};
