// --------------Navigation-----------------
export const assignmentsNavigation = {
  ASSIGNMENT_CARDS: "assignmentCardsScreen",
  ASSIGNMENT_QUESTIONS: "assignmentQuestionsSreen",
  ASSIGNMENT_SUBMISSION: "assignmentSubmissionScreen",
  ASSIGNMENT_SOLUTIONS: "assignmentSolutionsScreen",
};
// --------------Action Area-----------------
export const assignmentsUserActionArea = {
  ASSIGNMENT_CARD: "assignmentCard",
  BACK_BUTTON: "backButton",
  ASSIGNMENT_TAB: "assignmentTab",
  SOLUTIONS_TAB: "solutionsTab",
  SUBMISSION_TAB: "submissionTab",
  TAB_CHANGE: "tabChange",
};
// --------------Attributes-----------------
export const assignmentsAttributes = {
  ASSIGNMENT_DATE: "assignmentDate",
  ASSIGNMENT_ID: "assignmentId",
  ASSIGNMENT_NAME: "assignmentName",
  ASSIGNMENT_STATUS: "assignmentStatus",
  CHAPTER_NAME: "chapterName",
  DELIVERY_ID: "deliveryId",
  DURATION: "duration",
  SOLUTIONS_PDF_NAME: "solutionsPdfName",
  SOLUTIONS_PDF_URL: "solutionsPdfUrl",
  SOLUTIONS_VIDEO_NAME: "solutionsVideoName",
  SOLUTIONS_VIDEO_URL: "solutionsVideoUrl",
  SUBJECT_NAME: "subjectName",
  SUBMISSION_DEADLINE: "submissionDeadline",
  SUBMITTED_ON: "submittedOn",
  TOTAL_QUESTIONS: "totalQuestions",
};
