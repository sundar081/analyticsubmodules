// --------------Navigation-----------------
export const authenticationNavigation = {
  LOGIN_SCREEN: "loginScreen",
  CONFIRM_DETAIL: "confirmDetailScreen",
  SET_PASSWORD: "setPasswordScreen",
  LOGOUT_DIALOG: "logoutDialog",
  DEVICE_SWITCH_OTP_SCREEN: "deviceSwitchOtpScreen",
};
// --------------Action Area-----------------
export const authenticationUserActionArea = {
  FORGOT_PASSWORD: "forgotPassword",
  SHOW: "show",
  HIDE: "hide",
  LOGIN: "login",
  ADD: "add",
  REMOVE: "remove",
  CANCEL: "cancel",
  SAVE: "save",
  CONFIRM_AND_CONTINUE: "confirmAndContinue",
  CONTINUE: "continue",
  BACK_BUTTON: "backButton",
  YES: "yes",
  NO: "no",
  VERIFY: "verify",
};
// --------------Attributes-----------------
export const authenticationUserAttributes = {
  ADMISSION_NO: "admissionNo",
};
