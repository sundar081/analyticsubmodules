// --------------Action Area-----------------
export const cumulativeTopicAnalysisActionArea = {
  BACK_BUTTON: "backButton",
  CHOOSE_SUBJECT_DROPDOWN: "chooseSubjectDropdown",
  CHAPTER_TEST: "chapterTest",
  CHAPTER_PRACTICE: "chapterPractice",
  CHAPTER_EXPAND: "chapterExpand",
  CHAPTER_COLLAPSE: "chapterCollapse",
};
// --------------Attributes-----------------
export const cumulativeTopicAnalysisAttributes = {
  SUBJECT_ID: "subjectId",
  ANALYSIS_TYPE: "analysisType",
  CHAPTER_ID: "chapterId",
  CHAPTER_NAME: "chapterName",
};
