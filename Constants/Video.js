// --------------Navigation-----------------
export const videoScreenNavigation = {
  SUBJECTS: "subjectWiseVideoScreen",
  CHAPTER_WISE_VIDEOS: "chapterWiseVideoScreen",
  CHAPTERS: "chaptersVideoScreen",
  TOPIC_WISE_VIDEOS: "topicWiseVideoScreen",
  WATCH_VIDEO_SCREEN: "watchVideoScreen",
};
// --------------Action Area-----------------
export const videoActionArea = {
  START: "start",
};
// --------------Attributes-----------------
export const videoAttributes = {
  SUBJECT_ID: "subjectId",
  SUBJECT_NAME: "subjectName",
  CHAPTER_ID: "chapterId",
  CHAPTER_NAME: "chapterName",
  CONTENT_ID: "contentId",
  CONTENT_NAME: "contentName",
  REFERENCE: "reference",
  SOURCE: "source",
};
