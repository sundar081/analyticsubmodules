// --------------Event Types-----------------
export const analyticsEventType = {
    API_ERROR: "apiError",
    API_SUCCESS: "apiSuccess",
    USER_ACTION: "userAction",
    SCREEN_TIME_SPENT: "screenTimeSpent",
  };
  // --------------Event Groups-----------------
  export const analyticsEventGroup = {
    AUTHENTICATION: "authentication",
    FORGOT_PASSWORD: "forgotPassword",
    TABS_NAVIGATION: "tabsNavigation",
    PROFILE: "profile",
    GENERAL_ACTION: "generalAction",
    HELP: "help",
    PERFORMANCE: "performance",
    PRACTICE: "practice",
    SCHEDULED_TEST: "scheduledTest",
    API: "api",
    APP_ACTION: "appAction",
    HOME: "home",
    NLEARN_CORNER: "nlearnCorner",
    CUMULATIVE_ANALYSIS: "cumulativeAnalysis",
    VIDEO: "video",
    TEST: "test",
    LIVE_CLASSES: "live-classes",
    LEARN: "learn",
    ADDITIONAL_RESOURCES: "additionalResources",
    ONBOARDING: "onboarding",
    ASSIGNMENTS: "assignments",
    ANALYTICS_FEATURE: "analytics",
    DOUBT_SOLVING: "doubtSolving",
    LIBRARY: "library",
    SCHEDULE: "schedule",
    ANNOUNCEMENT: "announcement",
  };
  
  