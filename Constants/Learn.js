// --------------Navigation-----------------
export const learnScreenNavigation = {
  AUDIO_DIALOG: "audioDialog",
  ASSIGNMENTS: "assignmentsScreen",
  PDF_VIEWER: "pdfViewerScreen",
  LEARN_TOPIC: "learnTopicScreen",
  VIDEOS: "videoScreen",
  MATERIAL: "materialScreen",
  SUMMARY: "summaryScreen",
  QUESTION_ANSWER: "questionAnswerScreen",
  SINGLE_VIDEO: "singleVideoScreen",
  PDF: "pdfScreen",
};
// --------------Action Area-----------------
export const learnActionArea = {
  PDF_BUTTON: "openPdfButton",
  AUDIO_BUTTON: "playAudioButton",
  SUBJECT_DROPDOWN: "subjectDropDown",
  PENDING_TOPIC_CARD: "pendingTopicCard",
  TOPIC_CARD: "topicCard",
  TOPIC_TYPE_TAB: "topicTypeTab",
  VIEW_ALL: "viewAll",
  VIEW_LESS: "viewLess",
  VIDEO_CARD: "videoCard",
  QUESTION_CARD: "questionCard",
  EXPAND_ALL: "expandAll",
  COLLAPSE_ALL: "collapseAll",
  MATERIAL_CARD: "materialCard",
  BACK_BUTTON: "backButton",
  UPNEXT_CARD: "upNextCard",
  LIKE_BUTTON: "likeButton",
  DISLIKE_BUTTON: "dislikeButton",
  REPORT_BUTTON: "reportButton",
  SUBMIT_REPORT_BUTTON: "submitReportButon",
};
// --------------Attributes-----------------
export const learnAttributes = {
  PDF_BUTTON: "openPdfButton",
  AUDIO_BUTTON: "playAudioButton",
  SUBJECT_ID: "subjectId",
  TOPIC_ID: "topicId",
  CHAPTER_ID: "chapterId",
  VIDEO_ID: "videoId",
  QUESTION_ID: "questionId",
  MATERIAL_ID: "materialId",
};
