// --------------Navigation-----------------
export const scheduleNavigation = {
  SCHEDULE: "scheduleScreen",
};
// --------------Action Area-----------------
export const scheduleUserActionArea = {
  MONTH_DROPDOWN: "monthDropdown",
  NEXT_BUTTON: "nextButton",
  PREVIOUS_BUTTON: "previousButton",
  ZOOM_BUTTON: "zoomButton",
};
// --------------Attributes-----------------
export const scheduleAttributes = {
  MONTH_NAME: "monthName",
};
