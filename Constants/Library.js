// --------------Navigation-----------------
export const libraryFeatureNavigation = {
  RESOURCE_LIST: "resourceListScreen",
  PDF_VIEWER: "pdfViewerScreen",
  VIDEO: "videoScreen",
  SINGLE_VIDEO: "singleVideoScreen",
};
// --------------Action Area-----------------
export const libraryFeatureUserActionArea = {
  LIBRARY_NAV_TAB: "libraryNavTab",
  SUBJECT_DROPDOWN: "subjectDropdown",
  RESOURCE_CARD: "resourceCard",
  VIDEO_CARD: "videoCard",
  UPNEXT_CARD: "upNextCard",
};
// --------------Attributes-----------------
export const libraryFeatureAttributes = {
  CHAPTER_ID: "chapterId",
  CHAPTER_NAME: "chapterName",
  CREATED_DATE: "createdDate",
  LIBRARY_ID: "libraryId",
  LIBRARY_TYPE: "libraryType",
  PAGE_COUNT: "pageCount",
  RESOURCE_DATE: "resourceDate",
  RESOURCE_NAME: "resourceName",
  RESOURCE_URL: "resourceUrl",
  SUBJECT_ID: "subjectId",
  SUBJECT_NAME: "subjectName",
};
