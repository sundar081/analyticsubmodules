// --------------Navigation-----------------
export const homeScreenNavigation = {
  HOME: "homeScreen",
  HOME_HOME: "homeHomeScreen",
  HOME_TEST: "homeTestScreen",
  HOME_PRACTICE: "homePracticeScreen",
  HOME_PERFORMANCE_LOCKED: "homePerformanceLockedScreen",
  HOME_PERFORMANCE_UNLOCKED: "homePerformanceUnLockedScreen",
  HOME_VIDEO: "homeVideo",
  HOME_LIVE_CLASSES: "homeLiveClasses",
  HOME_ASSIGNMENTS: "homeAssignments",
  HOME_LIBRARY: "homeLibrary",
  HOME_LEARN: "homeLearn",
};
export const homeNavigation = {
  FEATURE_NOT_AVAILABLE_DIALOG: "homeHomeScreenDialog",
};
// --------------Action Area-----------------
export const homeScreenUserActionArea = {
  TAB_CHANGE: "tabChange",
  DROPDOWN: "dropdown",
  DROPDOWN_ITEM: "dropdownItem",
  SUBJECT_CARD: "subjectCardHomeScreen",
  CONTINUE_EXERCISE: "continueExercise",
  CONTINUE_VIDEO: "continueVideo",
  COURSE_SWITCH: "courseSwitch",
};
export const homeUserActionArea = {
  START_TEST: "startTest",
  RESUME: "resume",
  CLICK_TO_SEE: "clickToSee",
  VIEW_RESULT: "viewResult",
  START_PRACTICE: "startPractice",
  SIDEBAR_ITEM: "sidebarItem",
};
// --------------Attributes-----------------
export const homeScreenAttributes = {
  SUBJECT: "subject",
  EXERCISE_SUBJECT: "exerciseSubject",
  EXERCISE_CHAPTER: "exerciseChapter",
  EXERCISE_FORMAT: "exerciseFormat",
  EXERCISE_FORMAT_ID: "exerciseFormatId",
  EXERCISE_TYPE: "exerciseType",
  VIDEO_SUBJECT: "videoSubject",
  VIDEO_CHAPTER: "videoChapter",
  VIDEO_TOPIC: "videoTopic",
  VIDEO_SOURCE: "videoSource",
  VIDEO_ID: "videoId",
  VIDEO_LINK: "videoUrl",
  VIDEO_DURATION: "videoDuration",
  DROPDOWN: "dropdown",
  DROPDOWN_ITEM: "dropdownItem",
  COURSE_SWITCH_SELECT: "courseSwitchSelect",
};
export const homeAttributes = {
  TEST_ID: "testId",
  DELIVERY_ID: "deliveryId",
  EXAM_STATE: "examState",
  TEST_URI_ID: "testUriId",
  SUBJECT_ID: "subjectId",
  TOPIC_ID: "topicId",
  CHAPTER_ID: "chapterId",
  EXAM_CATEGORY: "examCategory",
};
