// --------------Navigation-----------------
export const forgotPasswordNavigation = {
  FORGOT_PASSWORD_ADMISSION_NO: "forgotPasswordAdmissionNumScreen",
  FORGOT_PASSWORD_MOBILE_OR_EMAIL: "forgotPasswordMobileOrEmailScreen",
  FORGOT_PASSWORD_MOBILE: "forgotPasswordMobileScreen",
  FORGOT_PASSWORD_EMAIL: "forgotpasswordEmailScreen",
  FORGOT_PASSWORD_ENTER_OTP: "forgotPasswordEnterOtpScreen",
  FORGOT_PASSWORD_RESET_PASSWORD: "forgotPasswordResetPasswordScreen",
  FORGOT_PASSWORD_CONGRATS: "forgotPasswordCongrats",
};
// --------------Action Area-----------------
export const forgotPasswordUserActionArea = {
  LOGIN: "login",
  BACK_BUTTON: "backButton",
  CONTINUE: "continue",
  MOBILE: "mobile",
  EMAIL: "email",
  DROPDOWN: "dropdown",
  GET_OTP: "getOtp",
  RESEND_OTP: "resendOtp",
  VERIFY: "verify",
};
// --------------Attributes-----------------
export const forgotPasswordAttributes = {
  ADMISSION_NO: "admissionNo",
};
