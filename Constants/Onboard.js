// --------------Navigation-----------------
export const onboardingNavigation = {
  ONBOARDING_RESET_PASSWORD: "onboardingResetPasswordScreen",
};
// --------------Action Area-----------------
export const onboardingUserActionArea = {
  CONTINUE: "continue",
};
// --------------Attributes-----------------
export const onboardingAttributes = {
  ADMISSION_NO: "admissionNo",
};
