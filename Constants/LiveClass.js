// --------------Navigation-----------------
export const liveClassesScreenNavigation = {
  LIVE_CLASSES: "liveClassesScreen",
  ZOOM_APP: "zoomApp",
  VIDEO_SCREEN: "videoScreen",
};
// --------------Action Area-----------------
export const liveClassesActionArea = {
  LIVE_VIDEO_BUTTON: "liveVideoButton",
  RECORDED_VIDEO_BUTTON: "recordedVideoButton",
  LIVE_VIDEO_CARD: "liveClassCard",
  RECORDED_VIDEO_CARD: "recordedVideoCard",
};
// --------------Attributes-----------------
export const liveClassesAttributes = {
  ATTENDED_STATUS: "attendedStatus",
  DURATION: "duration",
  IS_COMPLETED: "isCompleted",
  ISRECORDED: "isRecorded",
  LIVE_CLASS_DAY: "liveClassDay",
  LIVE_CLASS_ID: "liveClassId",
  LIVE_CLASS_STATUS: "liveClassStatus",
  LIVE_CLASS_URL: "liveClassUrl",
  LIVE_CLASS_DATE: "liveDateTime",
  LIVE_TIME: "live_time",
  MEETING_ID: "meetingId",
  NAME: "name",
  RECORDED_URL: "recordedUrl",
};
