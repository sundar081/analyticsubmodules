// --------------Navigation-----------------
export const nLearnCornerNavigation = {
  NLEARN_CORNER_HOME_SCREEN: "nlearnCornerHomeScreen",
  NLEARN_CORNER_DETAIL_SCREEN: "nlearnCornerDetailScreen",
  REPORT_CARD_PRACTICE_SCREEN: "reportCardPracticeScreen",
  REPORT_CARD_TEST_SCREEN: "reportCardTestScreen",
  TOPIC_ANALYSIS_SCREEN: "topicAnalysisScreen",
};
// --------------Action Area-----------------
export const nLearnCornerUserActionArea = {
  NLEARN_CORNER: "nlearnCorner",
  BACK_BUTTON: "backButton",
  NLEARN_CORNER_CARD: "nlearnCornerCard",
  NLEARN_CORNER_CARD_FAVORITE: "nlearnCornerCardFavorite",
  LOAD_MORE: "loadMore",
  FEEDBACK_LIKE: "feedbackLike",
  FEEDBACK_DISLIKE: "feedbackDislike",
};
// --------------Attributes-----------------
export const nLearnCornerAttributes = {
  ITEM_ID: "itemId",
  CATEGORY_ID: "categoryID",
};
