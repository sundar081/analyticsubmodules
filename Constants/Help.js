// --------------Navigation-----------------
export const helpNavigation = {
    HELP_FAQS: 'helpFaqsScreen',
    HELP_NLEARN_BENEFITS: 'helpNLearnBenefitsScreen',
    HELP_DEMO_VIDEO: 'helpDemoVideoScreen',
    HELP_REQUEST_A_DEMO: 'helpRequestADemoScreen',
    HELP_CONTACT_US: 'helpContactUs',
  };
// --------------Action Area-----------------
  export const helpUserActionArea = {
    FAQS: 'faqs',
    NLEARN_BENEFITS: 'nLearnBenefits',
    DEMO_VIDEO: 'demoVideo',
    REQUEST_A_DEMO: 'requestADemo',
    CONTACT_US: 'contactUs',
    SUBMIT: 'submit',
    ADD_SCREENSHOT: 'addScreenshot',
    REMOVE_SCREENSHOT: 'removeScreenshot',
    HAVE_YOU_READ_OUR_FAQ_YET: 'haveYouReadOurFaqYet',
  };
  