// --------------Navigation-----------------
export const announcementNavigation = {
  ANNOUNCEMENT: "announcementScreen",
  ANNOUNCEMENT_DETAIL: "announcementDetailScreen",
  VIDEO_MODAL: "videoModalScreen",
  PDF: "pdfScreen",
  IMAGE_MODAL: "imageScreen",
};
// --------------Action Area-----------------
export const announcementUserActionArea = {
  ANNOUNCEMENT_CARD: "announcementCard",
  VIEW_MORE: "viewMore",
  BACK_BUTTON: "backButton",
  CLOSE_BUTTON: "closeButton",
  VIDEO_CARD: "videoCard",
  IMAGE_CARD: "imageCard",
  RESOURCE_CARD: "resourceCard",
};
// --------------Attributes-----------------
export const announcementAttributes = {
  ANNOUNCEMENT_ID: "announcementId",
  ANNOUNCMENT_TYPE: "announcementType",
  ANNOUNCEMET_DATE: "announcementDate",
  ANNOUNCEMENT_NAME: "announcementName",
  PAGE_NUMBER: "pageNumber",
  VIDEO_NAME: "videoName",
  VIDEO_URL: "videoUrl",
  RESOURCE_NAME: "resourceName",
  RESOURCE_URL: "resourceUrl",
  IMAGE_URL: "imageUrl",
};
