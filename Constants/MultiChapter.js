// --------------Action Area-----------------
export const multichapterActionArea = {
  CREATE_TEST_BUTTON: "createTestButton",
  VIEW_ATTEMPTED: "viewAttempted",
  START_TEST: "startTest",
  NEXT_BUTTON: "nextButton",
  ATTEMPTED_EXAM_ITEM: "attemptedExamItem",
  RESUME: "resume",
};
// --------------Navigation-----------------
export const multiChapterNavigation = {
  INSTRUCTIONS: "multiChapterInstructionsScreen",
  SELECT_SUBJECT: "multiChapterSelectSubjectScreen",
  SELECT_CHAPTER: "multiChapterSelectChapterScreen",
  SELECT_FORMAT: "multiChapterSelectFormatScreen",
  REVIEW: "multiChapterReviewScreen",
  VIEW_ATTEMPTED: "multiChapterViewAttemptedScreen",
};
