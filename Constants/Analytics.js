// --------------Navigation-----------------
export const analyticsNavigationType = {
  NAVIGATION: "navigation",
  IN_SCREEN_NAVIGATION: "inScreenNavigation",
  SUB_NAVIGATION: "subNavigation",
};
export const analyticsFeatureNavigation = {
  PERFORMANCE: "performanceScreen",
  PROGRESS: "progressScreen",
  SCORE_GRAPH: "scoreGraph",
  RANK_GRAPH: "rankGraph",
  QUESTION_ATTEMPTED_GRAPH: "questionAttemptedGraph",
  AVG_TIME_PER_QUESTION_GRAPH: "avgTimePerQuestionGraph",
};
// --------------Action Area-----------------
export const appActionValues = {
  NETWORK_WIFI: "networkWifi",
  NETWORK_MOBILE: "networkMobile",
  NO_NETWORK: "noNetwork",
  APP_OPEN: "appOpen",
  APP_CLOSE: "appClose",
};
export const analyticsUserActionKeyType = {
  CLICK: "click",
  SCROLL_UP: "scrollUp",
  SCROLL_RIGHT: "scrollRight",
  SCROLL_LEFT: "scrollLeft",
  SCROLL_DOWN: "scrollDown",
  ZOOM_IN: "zoomIn",
  ZOOM_OUT: "zoomOut",
  SWIPE_LEFT: "swipeLeft",
  SWIPE_RIGHT: "swipeRightt",
  SWIPE_UP: "swipeUp",
  SWIPE_DOWN: "swipeDown",
  APP_ACTION: "appAction",
};
export const analyticsFeatureUserActionArea = {
  ANALYTICS_TYPE_TAB: "analyticsTypeTab",
  SUBJECT_DROPDOWN: "subjectDropdown",
  LEVEL_DROPDOWN: "levelDropdown",
  VIEW_RESULT: "viewResult",
};
// --------------Attributes-----------------
export const analyticsBaseAttributes = {
  PLATFORM: "platform",
  OS_VERSION: "osVersion",
  APP_VERSION: "appVersion",
  UUID: "uuid",
  STUDENT_ID: "studentId",
  ADMISSION_NUMBER: "admissionNo",
  EVENT_GROUP: "eventGroup",
  TIMESTAMP: "timeStamp",
  BROWSER_VERSION: "browserVersion",
  NETWORK_TYPE: "networkType",
};
export const analyticsApiErrorAttributes = {
  READ_TIME: "readTime",
  WRITE_TIME: "writeTime",
  ERROR_CODE: "errorCode",
  HEADERS: "headers",
  API_NAME: "apiName",
  ERROR_MESSAGE: "errorMessage",
};
export const analyticsFilterAttributes = {
  EVENT_TYPE: "eventType",
  USER_ACTION_KEY: "userActionKey",
  USER_NAVIGATION_KEY: "userNavigationKey",
  SCREEN_TIME_SPENT: "screenTimeSpent",
};
export const analyticsEndAttributes = {
  USER_ACTION_AREA: "userActionArea",
  FROM_NODE: "fromNode",
  TO_NODE: "toNode",
  VALIDATION_ERROR: "validationError",
  SCREEN_TIME_SPENT: "screenTimeSpent",
};
export const analyticsFeatureAttributes = {
  SUBJECT_ID: "subjectId",
  LEVEL_ID: "levelId",
};