// --------------Action Area-----------------
export const performanceUserActionArea = {
  PERFORMANCE_DATE: "date",
  PERFORMANCE_TEST_NAME: "testName",
  PERFORMANCE_EXAM_MODEL: "examModel",
  PERFORMANCE_AIR_RANK: "airRank",
  PERFORMANCE_SCORE: "score",
  PERFORMANCE_VIEW_RESULT: "testViewResult",
  PERFORMANCE_VIEW_TOPIC_ANALYSIS: "viewTopicAnalysis",
  PERFORMANCE_VIEW_ERROR_LIST: "viewErrorList",
  PERFORMANCE_BACK_BUTTON: "backButton",
  PERFORMANCE_ALL_QUESTION: "allQuestion",
  PERFORMANCE_ERRORS_ONLY: "errorsOnly",
  PERFORMANCE_SKIPPED_ONLY: "skippedOnly",
  PERFORMANCE_TOUGH_QUESTION_YOU_GOT_CORRECT: "toughQuestionYouGotCorrect",
  PERFORMANCE_EASY_QUESTION_YOU_DIDNT_GET_CORRECT:
    "easyQuestionYouDidNtGetCorrect",
  PERFORMANCE_QUESTION_NUMBER: "questionNumber",
  PERFORMANCE_STATUS: "status",
  PERFORMANCE_DIFFICULTY_LEVEL: "difficultyLevel",
  PERFORMANCE_SUBJECT_NAME: "subjectName",
  PERFORMANCE_ERROR_LIST_ITEM: "errorListItem",
  PERFORMANCE_ERROR_LIST_DIALOG_CLOSE_BUTTON: "errorListDialogCloseButton",
  PERFORMANCE_ERROR_LIST_DIALOG_EXPLANATION_LIKED:
    "errorListDialogExplanationLiked",
  PERFORMANCE_ERROR_LIST_DIALOG_EXPLANATION_DISLIKED:
    "errorListDialogExplanationDisliked",
};
// --------------Attributes-----------------
export const performanceAttributes = {
  EXAM_CATEGORY: "examCategory",
  TEST_ID: "testId",
  DELIVERY_ID: "deliveryId",
  ITEM_ID: "itemId",
  STUDENT_RESPONSE: "studentResponse",
  ANSWER_KEY: "answerKey",
  RESPONSE_ID: "responseId",
  TEST_NAME: "testName",
  ERROR_LIST_QUESTION_NUMBER: "errorListQuestionNumber",
  ERROR_LIST_QUESTION_STATUS: "errorListQuestionStatus",
};
