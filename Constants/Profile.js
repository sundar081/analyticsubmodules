// --------------Navigation-----------------
export const profileNavigation = {
  PROFILE_PERSONAL_AND_COLLEGE_INFO: "profilePersonalAndCollegeInfo",
  PROFILE_PERSONAL_INFO_REPORT_ERROR_DIALOG:"profilePersonalInfoReportErrorDialog",
  PROFILE_COLLEGE_INFO_REPORT_ERROR_DIALOG:"profileCollegeInfoReportErrorDialog",
  PROFILE_SET_PASSWORD: "profileSetPassword",
};
// --------------Action Area-----------------
export const profileUserActionArea = {
  PERSONAL_INFO: "personalInfo",
  COLLEGE_INFO: "collegeInfo",
  SET_PASSWORD: "setPassword",
  REPORT_ERROR: "reportError",
  SAVE_CHANGES: "saveChanges",
  SUBMIT_REPORT: "submitReport",
  CLOSE: "close",
  SAVE_PASSWORD: "savePassword",
};
