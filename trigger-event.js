import { getPlatform } from './Analytics'
import {getGeneralEventAttributes} from './Analytics'

const eventFeature ={
    redesign: ['home','auth','test'],
    cnaps:['analytics','announcement','home', 'learn', 'profile','practice','library'],
    kids:['test','live']
}

const isAbleToTrigger=(feature)=>{
    const platform= getPlatform()
    return (eventFeature[platform].includes(feature) || eventFeature[platform].includes('all'))
}

export const recordEvent=(params)=>{
    if(isAbleToTrigger(params.feature)){
        params.attr = {...params.attr, ...getGeneralEventAttributes()}
        console.log(params)
    }else{
        return false
    }
}
