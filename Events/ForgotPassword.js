import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType} from '../Constants/Analytics'
import {forgotPasswordAttributes, forgotPasswordUserActionArea, forgotPasswordNavigation} from '../Constants/ForgotPassword'
import {getCommonAttributes,getFilterAttributes,} from 'analytics';
import {recordEvent} from '../trigger-event'

//--------------------Admission Number Continue Handler---------------------------
export const RE_onAdmissionNumberPrimaryButtonClick=(admissionNumber)=>{
    const params={
        eventName:analyticsEventGroup.FORGOT_PASSWORD,
        attr:{
            [forgotPasswordAttributes.ADMISSION_NO]: admissionNumber,
            ...getCommonAttributes(
              forgotPasswordUserActionArea.CONTINUE,
              forgotPasswordNavigation.FORGOT_PASSWORD_ADMISSION_NO,
              forgotPasswordNavigation.FORGOT_PASSWORD_MOBILE_OR_EMAIL,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
          },
        feature:'forgotPassword'
    }
    recordEvent(params)
}


//--------------------Fogot Password OTP Verify Continue---------------------------
export const RE_onOtpScreenPrimaryButtonClick=(path)=>{
    const params={
        eventName:analyticsEventGroup.FORGOT_PASSWORD,
        attr:{
            [forgotPasswordAttributes.ADMISSION_NO]: path[2],
            ...getCommonAttributes(
              forgotPasswordUserActionArea.VERIFY,
              forgotPasswordNavigation.FORGOT_PASSWORD_ENTER_OTP,
              forgotPasswordNavigation.FORGOT_PASSWORD_RESET_PASSWORD,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
          },
        feature:'forgotPassword'
    }
    recordEvent(params)
}

//--------------------resend OTP---------------------------
export const RE_onResendTextClick=(path)=>{
    const params={
        eventName:analyticsEventGroup.FORGOT_PASSWORD,
        attr:{
            [forgotPasswordAttributes.ADMISSION_NO]: path[2],
            ...getCommonAttributes(
              forgotPasswordUserActionArea.RESEND_OTP,
              forgotPasswordNavigation.FORGOT_PASSWORD_ENTER_OTP,
              forgotPasswordNavigation.FORGOT_PASSWORD_ENTER_OTP,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
          },
        feature:'forgotPassword'
    }
    recordEvent(params)
}
//--------------------Send OTP---------------------------
export const RE_onSendOtpPrimaryButtonClick=(path)=>{
    const params={
        eventName:analyticsEventGroup.FORGOT_PASSWORD,
        attr:{
            [forgotPasswordAttributes.ADMISSION_NO]: path[2],
            ...getCommonAttributes(
              forgotPasswordUserActionArea.GET_OTP,
              forgotPasswordNavigation.FORGOT_PASSWORD_MOBILE,
              forgotPasswordNavigation.FORGOT_PASSWORD_ENTER_OTP,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
          },
        feature:'forgotPassword'
    }
    recordEvent(params)
}
