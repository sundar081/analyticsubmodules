import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType, analyticsEndAttributes} from '../Constants/Analytics'
import {getFilterAttributes} from 'analytics';
import {recordEvent} from '../trigger-event'

import {testAttributes, testUserActionArea, testNavigation} from '../Constants/Test'

//--------------------Profile Changes Save--------------------------
export const RE_onSaveChangesButtonClick=(testId, deliveryId)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]:testId[0],
            [testAttributes.DELIVERY_ID]: deliveryId[0],
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: 'scheduled_test',
            [testAttributes.TEST_NAME]:'',
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:testUserActionArea.RESPONSE_SHEET,
            [analyticsEndAttributes.FROM_NODE]: testNavigation.TEST_ENGINE_THANK_YOU,
            [analyticsEndAttributes.TO_NODE]:  testNavigation.TEST_ENGINE_RESPONSE_SHEET,
          },
        feature:'test'
    }
    recordEvent(params)
}
