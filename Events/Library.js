import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType} from '../Constants/Analytics'
import {libraryFeatureAttributes, libraryFeatureUserActionArea, libraryFeatureNavigation} from '../Constants/Library'
import {getFilterAttributes,getCommonAttributes} from 'analytics';
import {recordEvent} from '../trigger-event'

//--------------------Library resource card details---------------------------
export const RE_onResourceDataCardDetailsClick=(data)=>{
    const params={
        eventName:analyticsEventGroup.LIBRARY,
        attr:{
            [libraryFeatureAttributes.CHAPTER_ID]:data.chapter_id,
            [libraryFeatureAttributes.CHAPTER_NAME]:data.chapter_name,
            [libraryFeatureAttributes.CREATED_DATE]:data.created_date,
            [libraryFeatureAttributes.LIBRARY_ID]:data.library_id,
            [libraryFeatureAttributes.LIBRARY_TYPE]:data.library_type,
            [libraryFeatureAttributes.PAGE_COUNT]:data.resource_count,
            [libraryFeatureAttributes.RESOURCE_DATE]:data.resource_date,
            [libraryFeatureAttributes.RESOURCE_NAME]:data.resource_name,
            [libraryFeatureAttributes.RESOURCE_URL]:data.resource_url,
            [libraryFeatureAttributes.SUBJECT_ID]:data.subject_id,
            [libraryFeatureAttributes.SUBJECT_NAME]:data.subject_name,
            ...getCommonAttributes(
              libraryFeatureUserActionArea.RESOURCE_CARD,
              libraryFeatureNavigation.RESOURCE_LIST,
              libraryFeatureNavigation.PDF_VIEWER,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
          },
        feature:'library'
    }
    recordEvent(params)
}

//--------------------Library video click---------------------------
export const RE_onVideoCardClick=(video)=>{
    const params={
        eventName:analyticsEventGroup.LIBRARY,
        attr:{
            [libraryFeatureAttributes.CHAPTER_ID]:video.chapter_id,
            [libraryFeatureAttributes.CHAPTER_NAME]:video.chapter_name,
            [libraryFeatureAttributes.LIBRARY_ID]:video.library_id,
            [libraryFeatureAttributes.LIBRARY_TYPE]:video.library_type,
            [libraryFeatureAttributes.SUBJECT_ID]:video.subject_id,
            [libraryFeatureAttributes.SUBJECT_NAME]:video.subject_name,
            ...getCommonAttributes(
              libraryFeatureUserActionArea.UPNEXT_CARD,
              libraryFeatureNavigation.SINGLE_VIDEO,
              libraryFeatureNavigation.SINGLE_VIDEO,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
          },
        feature:'library'
    }
    recordEvent(params)
}

//--------------------Library video play card click---------------------------
export const RE_onVideoPlayCardClick=(video)=>{
    const params={
        eventName:analyticsEventGroup.LIBRARY,
        attr:{
            [libraryFeatureAttributes.CHAPTER_ID]:video.chapter_id,
            [libraryFeatureAttributes.CHAPTER_NAME]:video.chapter_name,
            [libraryFeatureAttributes.LIBRARY_ID]:video.library_id,
            [libraryFeatureAttributes.LIBRARY_TYPE]:video.library_type,
            [libraryFeatureAttributes.SUBJECT_ID]:video.subject_id,
            [libraryFeatureAttributes.SUBJECT_NAME]:video.subject_name,
            ...getCommonAttributes(
              libraryFeatureUserActionArea.VIDEO_CARD,
              libraryFeatureNavigation.VIDEO,
              libraryFeatureNavigation.SINGLE_VIDEO,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
          },
        feature:'library'
    }
    recordEvent(params)
}
