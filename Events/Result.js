import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType, analyticsEndAttributes} from '../Constants/Analytics'
import {getFilterAttributes,getCommonAttributes} from 'analytics';
import {recordEvent} from '../trigger-event'

import {testAttributes, testUserActionArea, testNavigation, testScreenActionArea, testScreenNavigation} from '../Constants/Test'
//--------------------Answer Key Handle Filter-------------------------t
export const RE_onChipClick=(testId, deliveryId, getCategory, e)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: testId[0],
            [testAttributes.DELIVERY_ID]: deliveryId[0],
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: getCategory(),
            [testAttributes.TEST_NAME]: '',
            [testAttributes.ERROR_LIST_FILTER_TYPE]: e.name,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            ...getCommonAttributes(
              testScreenActionArea.ERROR_LIST_FILTER,
              testScreenNavigation.ERROR_LIST,
              testScreenNavigation.ERROR_LIST,
              'success',
            ),
          },
        feature:'resultScreen'
    }
    recordEvent(params)
}

//--------------------Answer Key Handle Question Modal--------------------------
export const RE_onQustionModalHideClick=(e, getCategory)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: e.test_id,
            [testAttributes.DELIVERY_ID]: e.delivery_id,
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: getCategory(),
            [testAttributes.TEST_NAME]: '',
            [testAttributes.QUESTION_ANSWER]: e.student_response,
            [testAttributes.QUESTION_STATE]: e.answer_status,
            [testAttributes.SUBJECT_ID]: e.subject_id,
            [testAttributes.QUESTION_NUMBER]: e.questionnumber,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            ...getCommonAttributes(
              e.test_id
                ? testScreenActionArea.ERROR_LIST_QUESTION_CARD
                : testScreenActionArea.CLOSE_BUTTON,
              e.test_id
                ? testScreenNavigation.ERROR_LIST
                : testScreenNavigation.ERROR_LIST_QUESTION_MODAL,
              e.test_id
                ? testScreenNavigation.ERROR_LIST_QUESTION_MODAL
                : testScreenNavigation.ERROR_LIST,
              'success',
            ),
          },
        feature:'resultScreen'
    }
    recordEvent(params)
}

//--------------------Answer Key Next Question Modal--------------------------
export const RE_onQustionModalNextClick=(SelectedQuestion, getCategory)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: SelectedQuestion.test_id,
            [testAttributes.DELIVERY_ID]: SelectedQuestion.delivery_id,
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: getCategory(),
            [testAttributes.TEST_NAME]: '',
            [testAttributes.QUESTION_ANSWER]: SelectedQuestion.student_response,
            [testAttributes.QUESTION_STATE]: SelectedQuestion.answer_status,
            [testAttributes.SUBJECT_ID]: SelectedQuestion.subject_id,
            [testAttributes.QUESTION_NUMBER]: SelectedQuestion.questionnumber,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            ...getCommonAttributes(
              testScreenActionArea.ERROR_LIST_NEXT,
              testScreenNavigation.ERROR_LIST_QUESTION_MODAL,
              testScreenNavigation.ERROR_LIST_QUESTION_MODAL,
              'success',
            ),
          },
        feature:'resultScreen'
    }
    recordEvent(params)
}

//--------------------Answer Key Prvious Question Modal--------------------------
export const RE_onQustionModalPrevClick=(SelectedQuestion, getCategory)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: SelectedQuestion.test_id,
            [testAttributes.DELIVERY_ID]: SelectedQuestion.delivery_id,
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: getCategory(),
            [testAttributes.TEST_NAME]: '',
            [testAttributes.QUESTION_ANSWER]: SelectedQuestion.student_response,
            [testAttributes.QUESTION_STATE]: SelectedQuestion.answer_status,
            [testAttributes.SUBJECT_ID]: SelectedQuestion.subject_id,
            [testAttributes.QUESTION_NUMBER]: SelectedQuestion.questionnumber,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            ...getCommonAttributes(
              testScreenActionArea.ERROR_LIST_PREV,
              testScreenNavigation.ERROR_LIST_QUESTION_MODAL,
              testScreenNavigation.ERROR_LIST_QUESTION_MODAL,
              'success',
            ),
          },
        feature:'resultScreen'
    }
    recordEvent(params)
}


//--------------------Result Detailed Analysis Tab Change--------------------------
export const RE_onHeaderTabClick=(path, getCategory, getFromRouteContainer, getToRouteContainer, e)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: path[0],
            [testAttributes.DELIVERY_ID]: path[1],
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: getCategory(),
            [testAttributes.TEST_NAME]: '',
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsEventGroup.TABS_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:testScreenActionArea.ANALYTICS_TAB,
            [analyticsEndAttributes.FROM_NODE]: getFromRouteContainer(),
            [analyticsEndAttributes.TO_NODE]: getToRouteContainer(e),
          },
        feature:'resultScreen'
    }
    recordEvent(params)
}

//--------------------Result Detailed Analysis Go Back--------------------------
export const RE_onHeaderContentClick=(path, getCategory, getFromRouteContainer)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: path[0],
            [testAttributes.DELIVERY_ID]: path[1],
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: getCategory(),
            [testAttributes.TEST_NAME]: '',
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:testScreenActionArea.BACK_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: getFromRouteContainer(),
            [analyticsEndAttributes.TO_NODE]: testNavigation.TEST_ENGINE_RESULT,
          },
        feature:'resultScreen'
    }
    recordEvent(params)
}

//--------------------Result OverView Test Redirect Flag--------------------------
export const RE_onPageReload=(testId, deliveryId, getCategory)=>{  //useEffect
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: testId[0],
            [testAttributes.DELIVERY_ID]: deliveryId[0],
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: getCategory(),
            [testAttributes.TEST_NAME]: '',
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            ...getCommonAttributes(
              testUserActionArea.VIEW_RESULT,
              testNavigation.TEST_ENGINE_THANK_YOU,
              testNavigation.TEST_ENGINE_RESULT,
              'success',
            ),
          },
        feature:'resultScreen'
    }
    recordEvent(params)
}

//--------------------Result OverView Navigate to Detailed Analysis--------------------------
export const RE_onNavigateToDetailedAnalysisButtonClick=(testId, deliveryId, getCategory, getNavigationDetailedNavigationFrom)=>{  //useEffect
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: testId[0],
            [testAttributes.DELIVERY_ID]: deliveryId[0],
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: getCategory(),
            [testAttributes.TEST_NAME]: '',
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            ...getCommonAttributes(
              testScreenActionArea.VIEW_ANALYSIS,
              getNavigationDetailedNavigationFrom(),
              testScreenNavigation.ERROR_LIST,
              'success',
            ),
          },
        feature:'resultScreen'
    }
    recordEvent(params)
}

//--------------------Result OverView Go Back if test Redirect Flag is true--------------------------
export const RE_onHeaderContentGoBackClickByTestRedirectFlag=(testId, deliveryId, getCategory)=>{  //useEffect
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: testId[0],
            [testAttributes.DELIVERY_ID]: deliveryId[0],
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: getCategory(),
            [testAttributes.TEST_NAME]: '',
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            ...getCommonAttributes(
              testUserActionArea.BACK_BUTTON,
              testNavigation.TEST_ENGINE_RESULT,
              testNavigation.TEST_ENGINE_THANK_YOU,
              'success',
            ),
          },
        feature:'resultScreen'
    }
    recordEvent(params)
}

//--------------------Result OverView Go Back if path 2 inludes akc, bkc, missed, offline--------------------------
export const RE_onHeaderContentGoBackClickByPathInludes=(testId, deliveryId, getCategory)=>{  //useEffect
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: testId[0],
            [testAttributes.DELIVERY_ID]: deliveryId[0],
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: getCategory(),
            [testAttributes.TEST_NAME]: '',
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            ...getCommonAttributes(
              testUserActionArea.BACK_BUTTON,
              testNavigation.TEST_ENGINE_RESULT,
              testScreenNavigation.ATTEMPTED_TEST,
              'success',
            ),
          },
        feature:'resultScreen'
    }
    recordEvent(params)
}


//--------------------Result OverView Go Back on else condition--------------------------
export const RE_onHeaderContentGoBackClick=(testId, deliveryId, getCategory)=>{  //useEffect
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: testId[0],
            [testAttributes.DELIVERY_ID]: deliveryId[0],
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: getCategory(),
            [testAttributes.TEST_NAME]: '',
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            ...getCommonAttributes(
              testUserActionArea.BACK_BUTTON,
              testNavigation.TEST_ENGINE_RESULT,
              testScreenNavigation.ATTEMPTED_TEST,
              'success',
            ),
          },
        feature:'resultScreen'
    }
    recordEvent(params)
}

//--------------------Result OverView handle Dropdown--------------------------
export const RE_onLevelDropdownClick=(testId, deliveryId, getCategory, selectedSubjected)=>{  //useEffect
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: testId[0],
            [testAttributes.DELIVERY_ID]: deliveryId[0],
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: getCategory(),
            [testAttributes.TEST_NAME]: '',
            [testAttributes.SUBJECT_ID]: selectedSubjected.name,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            ...getCommonAttributes(
              testUserActionArea.SUBJECT_DROPDOWN,
              testNavigation.TEST_ENGINE_RESULT,
              testNavigation.TEST_ENGINE_RESULT,
              'success',
            ),
          },
        feature:'resultScreen'
    }
    recordEvent(params)
}

//--------------------Result Test Videos Open Video by clicking Video Card--------------------------
export const RE_onTestVideosVideoCardClick=(testId, deliveryId, getCategory, selectedSubjected)=>{  //useEffect
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: testId[0],
            [testAttributes.DELIVERY_ID]: deliveryId[0],
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: getCategory(),
            [testAttributes.TEST_NAME]: '',
            [testAttributes.SUBJECT_ID]: selectedSubjected.name,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            ...getCommonAttributes(
              testUserActionArea.SUBJECT_DROPDOWN,
              testNavigation.TEST_ENGINE_RESULT,
              testNavigation.TEST_ENGINE_RESULT,
              'success',
            ),
          },
        feature:'resultScreen'
    }
    recordEvent(params)
}

//--------------------Result Test Videos Popup Modal--------------------------
export const RE_onPopupModalClick=(testId, deliveryId, category, video)=>{  //useEffect
  const params={
      eventName:analyticsEventGroup.TEST,
      attr:{
        [testAttributes.TEST_ID]: testId[0],
        [testAttributes.DELIVERY_ID]: deliveryId[0],
        [testAttributes.TEST_URI_ID]: '',
        [testAttributes.EXAM_CATEGORY]: category,
        [testAttributes.TEST_NAME]: video.test_name,
        [testAttributes.VIDEO_ID]: video.test_video_id,
        [testAttributes.VIDEO_NAME]: video.name,
        [testAttributes.VIDEO_URL]: video.url,
        ...getFilterAttributes(
          analyticsEventType.USER_ACTION,
          analyticsUserActionKeyType.CLICK,
          analyticsNavigationType.IN_SCREEN_NAVIGATION,
        ),
        ...getCommonAttributes(
          testScreenActionArea.CLOSE_BUTTON,
          testScreenNavigation.VIDEO_SOLUTION_MODAL,
          testScreenNavigation.VIDEO_SOLUTIONS,
          'success',
        ),
      },
      feature:'resultScreen'
  }
  recordEvent(params)
}