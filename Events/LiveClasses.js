import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType} from '../Constants/Analytics'
import {getFilterAttributes, getCommonAttributes} from 'analytics';
import {recordEvent} from '../trigger-event'

import {forgotPasswordAttributes, forgotPasswordUserActionArea, forgotPasswordNavigation} from '../Constants/ForgotPassword'
import {onboardingUserActionArea, onboardingNavigation} from '../Constants/Onboard'
import {homeScreenNavigation} from '../Constants/Home'

//--------------------On boarding continue record forgot password event---------------------------
export const RE_onBoardingForgotPasswordPrimaryButtonClick=(path)=>{
    const params={
        eventName:analyticsEventGroup.FORGOT_PASSWORD,
        attr:{
            [forgotPasswordAttributes.ADMISSION_NO]: path[2],
            ...getCommonAttributes(
              forgotPasswordUserActionArea.CONTINUE,
              forgotPasswordNavigation.FORGOT_PASSWORD_RESET_PASSWORD,
              forgotPasswordNavigation.FORGOT_PASSWORD_CONGRATS,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
          },
        feature:'on-board'
    }
    recordEvent(params)
}

//--------------------End of the record event---------------------------
export const RE_onBoardingResetPasswordPrimaryButtonClick=(path)=>{
    const params={
        eventName:analyticsEventGroup.ONBOARDING,
        attr:{
            ...getCommonAttributes(
              onboardingUserActionArea.CONTINUE,
              onboardingNavigation.ONBOARDING_RESET_PASSWORD,
              homeScreenNavigation.HOME_HOME,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
          },
        feature:'on-board'
    }
    recordEvent(params)
}