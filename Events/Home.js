import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType} from '../Constants/Analytics'
import {homeScreenAttributes, homeScreenUserActionArea, homeScreenNavigation} from '../Constants/Home'
import {liveClassesScreenNavigation} from '../Constants/LiveClass'
import { testNavigation } from '../Constants/Test'
import {getCommonAttributes,getFilterAttributes,} from 'analytics';
import {recordEvent} from '../trigger-event'


//--------------------continue Watch---------------------------
export const RE_onVideoCardClick=(data)=>{
    const params={
        eventName: analyticsEventGroup.HOME, 
        attr:{
            [homeScreenAttributes.VIDEO_SUBJECT]:data.subject_name,
            [homeScreenAttributes.VIDEO_CHAPTER]:data.chapter_name,
            [homeScreenAttributes.VIDEO_SOURCE]:data.video_source,
            [homeScreenAttributes.VIDEO_ID]:data.video_id,
            [homeScreenAttributes.VIDEO_TOPIC]:data.topic_name,
            [homeScreenAttributes.VIDEO_DURATION]:data.duration,
            [homeScreenAttributes.VIDEO_LINK]:data.video_link,
            ...getCommonAttributes(
              homeScreenUserActionArea.CONTINUE_VIDEO,
              homeScreenNavigation.HOME,
              homeScreenNavigation.HOME_LEARN,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
          },
        feature:'home'
    }
    recordEvent(params)
}
//--------------------Exercise---------------------------
export const RE_onExerciseCardClick=(exercise)=>{
    const params={
        eventName: analyticsEventGroup.HOME, 
        attr:{
            [homeScreenAttributes.EXERCISE_SUBJECT]:exercise.subject_name,
            [homeScreenAttributes.EXERCISE_CHAPTER]:exercise.chapter_name,
            [homeScreenAttributes.EXERCISE_FORMAT]:exercise.practice_name,
            [homeScreenAttributes.EXERCISE_FORMAT_ID]:exercise.practice_format_id,
            [homeScreenAttributes.EXERCISE_TYPE]:exercise.practice_type,
            ...getCommonAttributes(
              homeScreenUserActionArea.CONTINUE_EXERCISE,
              homeScreenNavigation.HOME,
              homeScreenNavigation.HOME_PRACTICE,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
          },
        feature:'home'
    }
    recordEvent(params)
}

//-------------------Course Switch---------------------------
export const RE_changeCourseSwitch=(e)=>{
    const params={
        eventName: analyticsEventGroup.PROFILE,
        attr:{
            [homeScreenAttributes.COURSE_SWITCH_SELECT]: e.split('~')[1],
            ...getCommonAttributes(
              homeScreenUserActionArea.COURSE_SWITCH,
              homeScreenNavigation.HOME,
              homeScreenNavigation.HOME,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
          },
        feature:'home'
    }
    recordEvent(params)
}

//-------------------Learn Subject---------------------------
export const RE_onSubjectCardClick=(sb)=>{
    const params={
        eventName: analyticsEventGroup.HOME,
        attr:{
            [homeScreenAttributes.SUBJECT]: sb.subject_name,
            ...getCommonAttributes(
              homeScreenUserActionArea.SUBJECT_CARD,
              homeScreenNavigation.HOME,
              homeScreenNavigation.HOME_LEARN,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
          },
        feature:'home'
    }
    recordEvent(params)
}
//--------------------Live Class---------------------------
export const RE_onLiveCardWatchNowClick=(data)=>{
    const params={
        eventName:analyticsEventGroup.HOME,
        attr:{
            [homeScreenAttributes.VIDEO_SUBJECT]: data.subject_name,
            [homeScreenAttributes.VIDEO_CHAPTER]: data.chapter_name,
            [homeScreenAttributes.VIDEO_SOURCE]: data.video_source,
            [homeScreenAttributes.VIDEO_ID]: data.video_id,
            [homeScreenAttributes.VIDEO_TOPIC]: data.topic_name,
            [homeScreenAttributes.VIDEO_DURATION]: data.duration,
            [homeScreenAttributes.VIDEO_LINK]: data.video_link,
            ...getCommonAttributes(
              homeScreenUserActionArea.CONTINUE_VIDEO,
              homeScreenNavigation.HOME_LEARN,
              liveClassesScreenNavigation.ZOOM_APP,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
          },
        feature:'home'
    }
    recordEvent(params)
}

//--------------------Test Exam---------------------------
export const RE_onExamCardResultClick=(exam)=>{
    const params={
        evntaName: analyticsEventGroup.HOME,
        attr: {
          [homeScreenAttributes.DELIVERY_ID]: exam.delivery_id,
          [homeScreenAttributes.TEST_ID]: exam.test_id,
          [homeScreenAttributes.EXAM_STATE]: exam.exam_state,
          [homeScreenAttributes.TEST_URI_ID]: exam.test_uri_id,
          [homeScreenAttributes.EXAM_CATEGORY]: exam.exam_category,
          [homeScreenAttributes.TEST_NAME]: exam.exam_name,
          ...getCommonAttributes(
            homeScreenUserActionArea.START_TEST,
            homeScreenNavigation.HOME,
            testNavigation.TEST_INSTRUCTION,
            'success',
          ),
          ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
          ),
        },
        feature:'home',
    }
    recordEvent(params)
}

