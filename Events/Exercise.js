import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType, analyticsEndAttributes} from '../Constants/Analytics'
import {practiceNavigation, practiceUserActionArea} from '../Constants/Practice'
import {testScreenNavigation} from '../Constants/Test'
import {getFilterAttributes,} from 'analytics';
import {recordEvent} from '../trigger-event'

//--------------------Exercise Explanation Feedback---------------------------
export const RE_onExplanaionFeedClick=(value)=>{

    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
                analyticsEventType.USER_ACTION,
                analyticsUserActionKeyType.CLICK,
                analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: 'practiceExerciseQuestionExplanation',
            [analyticsEndAttributes.FROM_NODE]: 'practiceExerciseQuestionExplanation',
            [analyticsEndAttributes.TO_NODE]: `explanationFeedbackStatus-${value==1?'like':'dislike'}`,
        },
        feature:'practice'
    }
    recordEvent(params)
}

//--------------------Exercise Submit---------------------------
export const RE_onFilledButtonSubmitClick=(question)=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: 'exerciseSubmitButton',
            [analyticsEndAttributes.FROM_NODE]: question.questionnumber,
            [analyticsEndAttributes.TO_NODE]: question.questionnumber,
          },
        feature:'practice'
    }
    recordEvent(params)
}

//--------------------Exercise Skipped---------------------------
export const RE_onFilledButtonSkippedClick=(question)=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: 'exerciseSkippedButton',
            [analyticsEndAttributes.FROM_NODE]: question.questionnumber,
            [analyticsEndAttributes.TO_NODE]: question.questionnumber,
          },
        feature:'practice'
    }
    recordEvent(params)
}

//--------------------Next---------------------------
export const RE_onFooterNextClick=(qNum)=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: 'exerciseNextButton',
            [analyticsEndAttributes.FROM_NODE]: qNum,
            [analyticsEndAttributes.TO_NODE]: qNum+1,
        },
        feature:'practice'
    }
    recordEvent(params)
}

//--------------------Previous---------------------------
export const RE_onFooterPrevClick=(qNum)=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: 'exercisePreviousButton',
            [analyticsEndAttributes.FROM_NODE]: qNum,
            [analyticsEndAttributes.TO_NODE]: qNum-1,
        },
        feature:'practice'
    }
    recordEvent(params)
}

//--------------------Go To Chapter Page---------------------------
export const RE_onExerciseSummaryGotoChapterPageClick=(params)=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: params.testType==="chapter"?practiceNavigation.PRACTICE_CHAPTER_EXERCISE_SUMMARY_DIALOG:practiceNavigation.PRACTICE_TOPIC_EXERCISE_SUMMARY_DIALOG,
            [analyticsEndAttributes.FROM_NODE]: params.testType==="chapter"?practiceNavigation.PRACTICE_CHAPTER_EXERCISE_SUMMARY_DIALOG:practiceNavigation.PRACTICE_TOPIC_EXERCISE_SUMMARY_DIALOG,
            [analyticsEndAttributes.TO_NODE]: practiceNavigation.PRACTICE_TOPIC,
        },
        feature:'practice'
    }
    recordEvent(params)
}

//--------------------Report Error---------------------------
export const RE_onReportErroModalSubmit=(params)=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: "exerciseQuestionDetails",
            [analyticsEndAttributes.FROM_NODE]: 'exerciseQuestionScreen',
            [analyticsEndAttributes.TO_NODE]: params.testType==="chapter"?practiceNavigation.PRACTICE_CHAPTER_EXERCISE_REPORT_ERROR_DIALOG:practiceNavigation.PRACTICE_TOPIC_EXERCISE_REPORT_ERROR_DIALOG,
        },
        feature:'practice'
    }
    recordEvent(params)
}

//--------------------Handle Done---------------------------
export const RE_onHeaderHandleExerciseClick=(params)=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: practiceUserActionArea.PRACTICE_EXERCISE,
            [analyticsEndAttributes.FROM_NODE]: testScreenNavigation.PRACTICE_EXERCISE,
            [analyticsEndAttributes.TO_NODE]: params.testType==='chapter'? practiceNavigation.PRACTICE_CHAPTER_EXERCISE_LEVELS_DIALOG : practiceNavigation.PRACTICE_TOPIC_EXERCISE_SUMMARY_DIALOG,
          },
        feature:'practice'
    }
    recordEvent(params)
}