import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType} from '../Constants/Analytics'
import {getFilterAttributes,getCommonAttributes} from 'analytics';
import {recordEvent} from '../trigger-event'

import {homeScreenNavigation} from '../Constants/Home'
import { authenticationUserAttributes, authenticationUserActionArea, authenticationNavigation } from '../Constants/Authentication'

//--------------------Verify Continue-------------------------
export const RE_onPrimaryButtonClick=(admissionNumber)=>{
    const params={
        eventName:analyticsEventGroup.AUTHENTICATION,
        attr:{
            [authenticationUserAttributes.ADMISSION_NO]: admissionNumber,
            ...getCommonAttributes(
              authenticationUserActionArea.VERIFY,
              authenticationNavigation.DEVICE_SWITCH_OTP_SCREEN,
              homeScreenNavigation.HOME_HOME,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
          },
        feature:'auth'
    }
    recordEvent(params)
}
