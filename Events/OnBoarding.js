import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType} from '../Constants/Analytics'
import {getFilterAttributes,getCommonAttributes} from 'analytics';
import {recordEvent} from '../trigger-event'

import {liveClassesAttributes, liveClassesActionArea, liveClassesScreenNavigation} from '../Constants/LiveClass'

//--------------------Library resource card details---------------------------
export const RE_onResourceDataCardDetailsClick=(data)=>{
    const params={
        eventName:analyticsEventGroup.LIVE_CLASSES,
        attr:{
            [liveClassesAttributes.ATTENDED_STATUS]:data.attended_status,
            [liveClassesAttributes.DURATION]:data.duration,
            [liveClassesAttributes.IS_COMPLETED]:data.is_completed,
            [liveClassesAttributes.ISRECORDED]:data.is_recorded,
            [liveClassesAttributes.LIVE_CLASS_DAY]:data.live_class_day,
            [liveClassesAttributes.LIVE_CLASS_ID]:data.live_class_id,
            [liveClassesAttributes.LIVE_CLASS_STATUS]:data.live_class_status,
            [liveClassesAttributes.LIVE_CLASS_URL]:data.live_class_url,
            [liveClassesAttributes.LIVE_TIME]:data.live_time,
            [liveClassesAttributes.MEETING_ID]:data.meeting_id,
            ...getCommonAttributes(
              liveClassesActionArea.LIVE_VIDEO_CARD,
              liveClassesScreenNavigation.LIVE_CLASSES,
              data.is_recorded ? liveClassesScreenNavigation.VIDEO_SCREEN : liveClassesScreenNavigation.ZOOM_APP,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
          },
        feature:'liveClasses'
    }
    recordEvent(params)
}
