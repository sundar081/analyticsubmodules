import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType, analyticsEndAttributes} from '../Constants/Analytics'
import {doubtSolvingUserActionArea, doubtSolvingNavigation, doubtSolvingAttributes} from '../Constants/DoubtSolving'
import {getFilterAttributes,} from 'analytics';
import {recordEvent} from '../trigger-event'


//--------------------Hanlde Image---------------------------
export const RE_onDoubtImageClick=(activeSubject)=>{
    const params={
        eventName:analyticsEventGroup.DOUBT_SOLVING,
        attr:{
            [doubtSolvingAttributes.SUBJECT_ID]: activeSubject.subject_id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:doubtSolvingUserActionArea.SUBJECT_BUTTON,
            [analyticsEndAttributes.FROM_NODE]:doubtSolvingNavigation.ASK_DOUBT_MODAL,
            [analyticsEndAttributes.TO_NODE]: doubtSolvingNavigation.ASK_DOUBT_MODAL,
          },
        feature:'doubtSolving'
    }
    recordEvent(params)
}

//--------------------Hanlde Doubt---------------------------
export const RE_onDoubtButtonClick=(activeSubject)=>{
    const params={
        eventName:analyticsEventGroup.DOUBT_SOLVING,
        attr:{
            [doubtSolvingAttributes.SUBJECT_ID]: activeSubject.subject_id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:doubtSolvingUserActionArea.ASK_DOUBT,
            [analyticsEndAttributes.FROM_NODE]:doubtSolvingNavigation.ASK_DOUBT_MODAL,
            [analyticsEndAttributes.TO_NODE]: doubtSolvingNavigation.ASK_DOUBT_MODAL,
          },
        feature:'doubtSolving'
    }
    recordEvent(params)
}

//--------------------Remove Image---------------------------
export const RE_onDoubtImageRemoveClick=(activeSubject)=>{
    const params={
        eventName:analyticsEventGroup.DOUBT_SOLVING,
        attr:{
            [doubtSolvingAttributes.SUBJECT_ID]: activeSubject.subject_id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:doubtSolvingUserActionArea.SUBJECT_BUTTON,
            [analyticsEndAttributes.FROM_NODE]:doubtSolvingNavigation.ASK_DOUBT_MODAL,
            [analyticsEndAttributes.TO_NODE]: doubtSolvingNavigation.ASK_DOUBT_MODAL,
          },
        feature:'doubtSolving'
    }
    recordEvent(params)
}

//--------------------Select Subject---------------------------
export const RE_onSelectSubjectButtonClick=(sub)=>{
    const params={
        eventName:analyticsEventGroup.DOUBT_SOLVING,
        attr:{
            [doubtSolvingAttributes.SUBJECT_ID]: sub.subject_id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:doubtSolvingUserActionArea.SUBJECT_BUTTON,
            [analyticsEndAttributes.FROM_NODE]:doubtSolvingNavigation.ASK_DOUBT_MODAL,
            [analyticsEndAttributes.TO_NODE]: doubtSolvingNavigation.ASK_DOUBT_MODAL,
          },
        feature:'doubtSolving'
    }
    recordEvent(params)
}

//--------------------Show Edit Doubt---------------------------
export const RE_onDoubtCardShowEditPopupClick=(data)=>{
    const params={
        eventName:analyticsEventGroup.DOUBT_SOLVING,
        attr:{
            [doubtSolvingAttributes.SUBJECT_ID]: data.subject_id,
            [doubtSolvingAttributes.DOUBT_ID]: data.doubt_id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:doubtSolvingUserActionArea.EDIT_DOUBT,
            [analyticsEndAttributes.FROM_NODE]:doubtSolvingNavigation.OPEN,
            [analyticsEndAttributes.TO_NODE]: doubtSolvingNavigation.ASK_DOUBT_MODAL,
          },
        feature:'doubtSolving'
    }
    recordEvent(params)
}

//--------------------Resolved Show---------------------------
export const reShowResolvedDoubts=(data)=>{
    const params={
        eventName:analyticsEventGroup.DOUBT_SOLVING,
        attr:{
            [doubtSolvingAttributes.SUBJECT_ID]: data.subject_id,
            [doubtSolvingAttributes.DOUBT_ID]: data.doubt_id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:doubtSolvingUserActionArea.VIEW_SOLUTION,
            [analyticsEndAttributes.FROM_NODE]:doubtSolvingNavigation.RESOVLED,
            [analyticsEndAttributes.TO_NODE]: doubtSolvingNavigation.SOLUTION_MODAL,
          },
        feature:'doubtSolving'
    }
    recordEvent(params)
}


//--------------------Handle Close---------------------------
export const RE_onSolutionModalClose=(getFromRouteContainer)=>{
    const params={
        eventName:analyticsEventGroup.DOUBT_SOLVING,
        attr:{
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:doubtSolvingUserActionArea.CLOSE_DOUBT,
            [analyticsEndAttributes.FROM_NODE]:doubtSolvingNavigation.ASK_DOUBT_MODAL,
            [analyticsEndAttributes.TO_NODE]: getFromRouteContainer(),
          },
        feature:'doubtSolving'
    }
    recordEvent(params)
}

//--------------------Handle Show---------------------------
export const RE_onDoubtCardViewSolutionClick=(getFromRouteContainer)=>{
    const params={
        eventName:analyticsEventGroup.DOUBT_SOLVING,
        attr:{
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:doubtSolvingUserActionArea.ASK_DOUBT_CARD,
            [analyticsEndAttributes.FROM_NODE]:getFromRouteContainer(),
            [analyticsEndAttributes.TO_NODE]: doubtSolvingNavigation.ASK_DOUBT_MODAL,
          },
        feature:'doubtSolving'
    }
    recordEvent(params)
}

//--------------------Tab Change---------------------------
export const RE_onHeaderTabsSelect=(getFromRouteContainer, getToRouteContainer, key)=>{
    const params={
        eventName:analyticsEventGroup.DOUBT_SOLVING,
        attr:{
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsEventGroup.TABS_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:doubtSolvingUserActionArea.DOUBT_TYPE_TAB,
            [analyticsEndAttributes.FROM_NODE]: getFromRouteContainer(),
            [analyticsEndAttributes.TO_NODE]: getToRouteContainer(key),
          },
        feature:'doubtSolving'
    }
    recordEvent(params)
}

//--------------------Subject Change---------------------------
export const RE_onSubjectDropdownSelect=(selectedSubjected,getFromRouteContainer, getToRouteContainer, tabkey)=>{
    const params={
        eventName:analyticsEventGroup.DOUBT_SOLVING,
        attr:{
            [doubtSolvingAttributes.SUBJECT_ID]: selectedSubjected.subject_id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:doubtSolvingUserActionArea.SUBJECT_DROPDOWN,
            [analyticsEndAttributes.FROM_NODE]:getFromRouteContainer(),
            [analyticsEndAttributes.TO_NODE]: getToRouteContainer(tabkey),
          },
        feature:'doubtSolving'
    }
    recordEvent(params)
}

