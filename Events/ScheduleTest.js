import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType, analyticsEndAttributes} from '../Constants/Analytics'
import {getFilterAttributes} from 'analytics';
import {recordEvent} from '../trigger-event'

import {testAttributes, testUserActionArea, testScreenNavigation, testNavigation, testScreenActionArea} from '../Constants/Test'

//--------------------Schedule Test Hanlde Result-------------------------
export const RE_onExamCardResultClick=(test_id, delivery_id, test_name)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: test_id,
            [testAttributes.DELIVERY_ID]: delivery_id,
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: 'scheduled_test',
            [testAttributes.TEST_NAME]: test_name,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:testUserActionArea.VIEW_RESULT,
            [analyticsEndAttributes.FROM_NODE]:testScreenNavigation.ATTEMPTED_TEST,
            [analyticsEndAttributes.TO_NODE]: testScreenNavigation.RESULT,
          },
        feature:'schedule-tests'
    }
    recordEvent(params)
}

//--------------------Schedule Hanlde Respnse Sheet-------------------------
export const RE_onExamCardResponseSheetClick=(test_id, delivery_id, val)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: test_id,
            [testAttributes.DELIVERY_ID]: delivery_id,
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: 'scheduled_test',
            [testAttributes.TEST_NAME]: val.test_name,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:testUserActionArea.RESPONSE_SHEET,
            [analyticsEndAttributes.FROM_NODE]:testScreenNavigation.ATTEMPTED_TEST,
            [analyticsEndAttributes.TO_NODE]:testNavigation.TEST_ENGINE_RESPONSE_SHEET,
          },
        feature:'schedule-tests'
    }
    recordEvent(params)
}

//--------------------Schedule Missed on Select------------------------
export const RE_onFilterByClick=(e, testMode, analysisType, getFromRouteContainer, getToRouteContainer)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.EXAM_CATEGORY]: 'scheduled_test',
            [testAttributes.EXAM_TYPE]: e,
            [testAttributes.EXAM_MODE]: testMode,
            [testAttributes.ANALYSIS_TYPE]: analysisType,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsEventGroup.TABS_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: testScreenActionArea.EXAM_TYPE_TAB,
            [analyticsEndAttributes.FROM_NODE]: getFromRouteContainer(),
            [analyticsEndAttributes.TO_NODE]: getToRouteContainer(e),
          },
        feature:'schedule-tests'
    }
    recordEvent(params)
}

//--------------------Schedule Missed handle Test Mode------------------------
export const RE_onFilterByTestModeClick=(activeTab, e, analysisType, getFromRouteContainer, getToRouteContainer)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.EXAM_CATEGORY]: 'scheduled_test',
            [testAttributes.EXAM_TYPE]: activeTab,
            [testAttributes.EXAM_MODE]: e,
            [testAttributes.ANALYSIS_TYPE]: analysisType,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsEventGroup.TABS_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:testScreenActionArea.EXAM_MODE_TAB,
            [analyticsEndAttributes.FROM_NODE]: getFromRouteContainer(),
            [analyticsEndAttributes.TO_NODE]: getToRouteContainer(activeTab),
          },
        feature:'schedule-tests'
    }
    recordEvent(params)
}

//--------------------Schedule Missed handle Test Type------------------------
export const RE_onFilterByAnalysisTypeClick=(activeTab, testMode, e, getFromRouteContainer, getToRouteContainer)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.EXAM_CATEGORY]: 'scheduled_test',
            [testAttributes.EXAM_TYPE]: activeTab,
            [testAttributes.EXAM_MODE]: testMode,
            [testAttributes.ANALYSIS_TYPE]: e,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsEventGroup.TABS_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:testScreenActionArea.ANALYIS_TYPE_TAB,
            [analyticsEndAttributes.FROM_NODE]: getFromRouteContainer(),
            [analyticsEndAttributes.TO_NODE]: getToRouteContainer(activeTab),
          },
        feature:'schedule-tests'
    }
    recordEvent(params)
}