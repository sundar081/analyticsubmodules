import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType, analyticsEndAttributes} from '../Constants/Analytics'
import { learnAttributes, learnActionArea, learnScreenNavigation } from '../Constants/Learn'
import {getFilterAttributes,} from 'analytics';
import {recordEvent} from '../trigger-event'


//--------------------Material PDF---------------------------
export const RE_onResourceCardPdfClick=(material)=>{
    const params={
        eventName:analyticsEventGroup.LEARN,
        attr:{
            [learnAttributes.SUBJECT_ID]: material.subject_id,
            [learnAttributes.CHAPTER_ID]: material.chapter_id,
            [learnAttributes.MATERIAL_ID]: material.resource_id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: learnActionArea.MATERIAL_CARD,
            [analyticsEndAttributes.FROM_NODE]: learnScreenNavigation.MATERIAL,
            [analyticsEndAttributes.TO_NODE]:  learnScreenNavigation.PDF,
          },
        feature:'learn'
    }
    recordEvent(params)
}


//--------------------Question answer Open all Flag---------------------------
export const RE_onExpandTextAllOpenFlagClick=(props)=>{
    const params={
        eventName:analyticsEventGroup.LEARN,
        attr:{
            [learnAttributes.SUBJECT_ID]: props.qa.data[0].subject_id,
            [learnAttributes.CHAPTER_ID]: props.qa.data[0].chapter_id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: learnActionArea.EXPAND_ALL,
            [analyticsEndAttributes.FROM_NODE]: learnScreenNavigation.QUESTION_ANSWER,
            [analyticsEndAttributes.TO_NODE]:  learnScreenNavigation.QUESTION_ANSWER,
          },
        feature:'learn'
    }
    recordEvent(params)
}

//--------------------All Close Icon---------------------------
export const RE_onExpandTextAllCloseIconClick=(props)=>{
    const params={
        eventName:analyticsEventGroup.LEARN,
        attr:{
            [learnAttributes.SUBJECT_ID]: props.qa.data[0].subject_id,
            [learnAttributes.CHAPTER_ID]: props.qa.data[0].chapter_id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: learnActionArea.COLLAPSE_ALL,
            [analyticsEndAttributes.FROM_NODE]: learnScreenNavigation.QUESTION_ANSWER,
            [analyticsEndAttributes.TO_NODE]:  learnScreenNavigation.QUESTION_ANSWER,
          },
        feature:'learn'
    }
    recordEvent(params)
}

//--------------------Toggle Open Flag---------------------------
export const RE_onQuestionCardClick=(props, index)=>{
    const params={
        eventName:analyticsEventGroup.LEARN,
        attr:{
            [learnAttributes.SUBJECT_ID]: props.qa.data[index].subject_id,
            [learnAttributes.CHAPTER_ID]: props.qa.data[index].chapter_id,
            [learnAttributes.QUESTION_ID]: props.qa.data[index].qa_id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: learnActionArea.QUESTION_CARD,
            [analyticsEndAttributes.FROM_NODE]: learnScreenNavigation.QUESTION_aNSWER,
            [analyticsEndAttributes.TO_NODE]:  learnScreenNavigation.QUESTION_ANSWER,
          },
        feature:'learn'
    }
    recordEvent(params)
}


//--------------------Learn Card Topic select---------------------------
export const RE_onLearnCardTopicClick=(topic)=>{
    const params={
        eventName:analyticsEventGroup.LEARN,
        attr:{
            [learnAttributes.SUBJECT_ID]: topic.subject_id,
            [learnAttributes.CHAPTER_ID]: topic.chapter_id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:learnActionArea.TOPIC_CARD,
            [analyticsEndAttributes.FROM_NODE]: learnScreenNavigation.LEARN_TOPIC,
            [analyticsEndAttributes.TO_NODE]:  learnScreenNavigation.VIDEOS,
          },
        feature:'learn'
    }
    recordEvent(params)
}
//--------------------Single Video Click---------------------------
export const RE_onVideoPlayCardVideoClick=(subjectId,chapterId,videoId,nextTopicId)=>{
    const params={
        eventName:analyticsEventGroup.LEARN,
        attr:{
            [learnAttributes.SUBJECT_ID]: subjectId,
            [learnAttributes.CHAPTER_ID]: chapterId,
            [learnAttributes.TOPIC_ID]: nextTopicId,
            [learnAttributes.VIDEO_ID]: videoId,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:learnActionArea.UPNEXT_CARD,
            [analyticsEndAttributes.FROM_NODE]: learnScreenNavigation.SINGLE_VIDEO,
            [analyticsEndAttributes.TO_NODE]:  learnScreenNavigation.SINGLE_VIDEO,
          },
        feature:'learn'
    }
    recordEvent(params)
}

//--------------------Head Content Topic Go Back---------------------------
export const RE_onHeadContentGoBackClick=(getFromRouteContainer)=>{
    const params={
        eventName:analyticsEventGroup.LEARN,
        attr:{
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: learnActionArea.BACK_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: getFromRouteContainer(),
            [analyticsEndAttributes.TO_NODE]: learnScreenNavigation.LEARN_TOPIC,
          },
        feature:'learn'
    }
    recordEvent(params)
}

//--------------------Header Tabs Topic Details Select Type---------------------------
export const RE_onHeaderTabsSelect=(getFromRouteContainer,getToRouteContainer, val)=>{
    const params={
        eventName:analyticsEventGroup.LEARN,
        attr:{
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsEventGroup.TABS_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:learnActionArea.TOPIC_TYPE_TAB,
            [analyticsEndAttributes.FROM_NODE]: getFromRouteContainer(),
            [analyticsEndAttributes.TO_NODE]: getToRouteContainer(val),
          },
        feature:'learn'
    }
    recordEvent(params)
}

//--------------------Topic Videos Click---------------------------
export const RE_onVideosClick=(chapter,video)=>{
    const params={
        eventName:analyticsEventGroup.LEARN,
        attr:{
            [learnAttributes.SUBJECT_ID]: chapter.subject_id,
            [learnAttributes.CHAPTER_ID]: chapter.chapter_id,
            [learnAttributes.TOPIC_ID]: chapter.topic_id,
            [learnAttributes.VIDEO_ID]: video.content_id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: learnActionArea.VIDEO_CARD,
            [analyticsEndAttributes.FROM_NODE]: learnScreenNavigation.VIDEOS,
            [analyticsEndAttributes.TO_NODE]:  learnScreenNavigation.SINGLE_VIDEO,
          },
        feature:'learn'
    }
    recordEvent(params)
}

