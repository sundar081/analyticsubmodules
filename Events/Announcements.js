import {analyticsEventGroup, analyticsEventType} from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType, analyticsEndAttributes} from '../Constants/Analytics'
import {announcementAttributes, announcementUserActionArea, announcementNavigation} from '../Constants/Announcements';
import {getFilterAttributes,} from 'analytics';
import {recordEvent} from '../trigger-event'

//--------------------Announcement Video Click---------------------------
export const RE_VideoCardVideoClick=(video, videoUrl)=>{
    const params={
        eventName:analyticsEventGroup.ANNOUNCEMENT,
        attr:{
            [announcementAttributes.VIDEO_NAME]: video.video_name,
            [announcementAttributes.VIDEO_URL]: videoUrl,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: announcementUserActionArea.VIDEO_CARD,
            [analyticsEndAttributes.FROM_NODE]: announcementNavigation.ANNOUNCEMENT_DETAIL,
            [analyticsEndAttributes.TO_NODE]:  announcementNavigation.VIDEO_MODAL,
          },
        feature:'announcement'
    }
    recordEvent(params)
}

//--------------------Announcement Close---------------------------
export const RE_popModalClose=(videoData, videoUrl)=>{
    const params={
        eventName:analyticsEventGroup.ANNOUNCEMENT,
        attr:{
            [announcementAttributes.VIDEO_NAME]: videoData.video_name,
            [announcementAttributes.VIDEO_URL]: videoUrl,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:announcementUserActionArea.CLOSE_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: announcementNavigation.VIDEO_MODAL,
            [analyticsEndAttributes.TO_NODE]:  announcementNavigation.ANNOUNCEMENT_DETAIL,
          },
        feature:'announcement'
    }
    recordEvent(params)
}

//--------------------Announcement Open PDF---------------------------
export const RE_onPdfWrapClick=(pdfName, pdfURL)=>{
    const params={
        eventName:analyticsEventGroup.ANNOUNCEMENT,
        attr:{
            [announcementAttributes.RESOURCE_NAME]: pdfName,
            [announcementAttributes.RESOURCE_URL]: pdfURL,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:announcementUserActionArea.RESOURCE_CARD,
            [analyticsEndAttributes.FROM_NODE]: announcementNavigation.ANNOUNCEMENT_DETAIL,
            [analyticsEndAttributes.TO_NODE]:  announcementNavigation.PDF,
          },
        feature:'announcement'
    }
    recordEvent(params)
}

//--------------------Announcement Handle Image---------------------------
export const RE_onImageClick=(im)=>{
    const params={
        eventName:analyticsEventGroup.ANNOUNCEMENT,
        attr:{
            [announcementAttributes.IMAGE_URL]: im,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:announcementUserActionArea.IMAGE_CARD,
            [analyticsEndAttributes.FROM_NODE]: announcementNavigation.ANNOUNCEMENT_DETAIL,
            [analyticsEndAttributes.TO_NODE]:  announcementNavigation.IMAGE_MODAL,
          },
        feature:'announcement'
    }
    recordEvent(params)
}

//--------------------Announcement---------------------------
export const RE_onAnnouncementCardClick=(data)=>{
    const params={
        eventName:analyticsEventGroup.ANNOUNCEMENT,
        attr:{
            [announcementAttributes.ANNOUNCEMENT_ID]: data.announcement_id,
            [announcementAttributes.ANNOUNCMENT_TYPE]: data.announcement_type,
            [announcementAttributes.ANNOUNCEMET_DATE]: data.announcement_date,
            [announcementAttributes.ANNOUNCEMENT_NAME]: data.heading,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: announcementUserActionArea.ANNOUNCEMENT_CARD,
            [analyticsEndAttributes.FROM_NODE]: announcementNavigation.ANNOUNCEMENT,
            [analyticsEndAttributes.TO_NODE]:  announcementNavigation.ANNOUNCEMENT_DETAIL,
          },
        feature:'announcement'
    }
    recordEvent(params)
}

//--------------------View More---------------------------
export const RE_onNextPageButtonClick=(currentPage)=>{
    const params={
        eventName:analyticsEventGroup.ANNOUNCEMENT,
        attr:{
            [announcementAttributes.PAGE_NUMBER]: currentPage,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: announcementUserActionArea.VIEW_MORE,
            [analyticsEndAttributes.FROM_NODE]: announcementNavigation.ANNOUNCEMENT,
            [analyticsEndAttributes.TO_NODE]:  announcementNavigation.ANNOUNCEMENT,
          },
        feature:'announcement'
    }
    recordEvent(params)
}

//--------------------Go Back---------------------------
export const RE_onHeadContentGoBackClick=()=>{
    const params={
        eventName:analyticsEventGroup.ANNOUNCEMENT,
        attr:{
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:announcementUserActionArea.BACK_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: announcementNavigation.ANNOUNCEMENT_DETAIL,
            [analyticsEndAttributes.TO_NODE]:  announcementNavigation.ANNOUNCEMENT,
          },
        feature:'announcement'
    }
    recordEvent(params)
}