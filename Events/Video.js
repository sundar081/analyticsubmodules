import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType, analyticsEndAttributes} from '../Constants/Analytics'
import {getFilterAttributes} from 'analytics';
import {recordEvent} from '../trigger-event'

import {learnAttributes, learnScreenNavigation, learnActionArea} from '../Constants/Learn'

//--------------------Video Go Back-------------------------
export const RE_onHeadingGoBackClick=(videoData, videoId)=>{
    const params={
        eventName:analyticsEventGroup.VIDEO,
        attr:{
            [learnAttributes.SUBJECT_ID]: videoData.subject_id,
            [learnAttributes.CHAPTER_ID]: videoData.chapter_id,
            [learnAttributes.TOPIC_ID]: videoData.topic_id,
            [learnAttributes.VIDEO_ID]: videoId,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:
            learnActionArea.BACK_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: learnScreenNavigation.SINGLE_VIDEO,
            [analyticsEndAttributes.TO_NODE]:  learnScreenNavigation.VIDEOS,
          },
        feature:'video'
    }
    recordEvent(params)
}

//--------------------Video Like Handler-------------------------
export const RE_onLikeWrapLikeIconClick=(videoId, videoData)=>{
    const params={
        eventName:analyticsEventGroup.VIDEO,
        attr:{
            [learnAttributes.SUBJECT_ID]: videoData.subject_id,
            [learnAttributes.CHAPTER_ID]: videoData.chapter_id,
            [learnAttributes.TOPIC_ID]: videoData.topic_id,
            [learnAttributes.VIDEO_ID]: videoId,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:
            learnActionArea.LIKE_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: learnScreenNavigation.SINGLE_VIDEO,
            [analyticsEndAttributes.TO_NODE]:  learnScreenNavigation.SINGLE_VIDEO,
          },
        feature:'video'
    }
    recordEvent(params)
}

//--------------------Video Dislike Handler-------------------------
export const RE_onLikeWrapDislikeIconClick=(videoId, videoData)=>{
    const params={
        eventName:analyticsEventGroup.VIDEO,
        attr:{
            [learnAttributes.SUBJECT_ID]: videoData.subject_id,
            [learnAttributes.CHAPTER_ID]: videoData.chapter_id,
            [learnAttributes.TOPIC_ID]: videoData.topic_id,
            [learnAttributes.VIDEO_ID]: videoId,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: learnActionArea.DISLIKE_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: learnScreenNavigation.SINGLE_VIDEO,
            [analyticsEndAttributes.TO_NODE]:  learnScreenNavigation.SINGLE_VIDEO,
          },
        feature:'video'
    }
    recordEvent(params)
}

//--------------------Video Report Handler-------------------------
export const RE_onLikeWrapReportIconClick=(videoId, videoData)=>{
    const params={
        eventName:analyticsEventGroup.VIDEO,
        attr:{
            [learnAttributes.SUBJECT_ID]: videoData.subject_id,
            [learnAttributes.CHAPTER_ID]: videoData.chapter_id,
            [learnAttributes.TOPIC_ID]: videoData.topic_id,
            [learnAttributes.VIDEO_ID]: videoId,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: learnActionArea.REPORT_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: learnScreenNavigation.SINGLE_VIDEO,
            [analyticsEndAttributes.TO_NODE]:  learnScreenNavigation.SINGLE_VIDEO,
          },
        feature:'video'
    }
    recordEvent(params)
}

//--------------------Video Submit Report-------------------------
export const RE_onReportErrorModalClick=(videoId, videoData)=>{
    const params={
        eventName:analyticsEventGroup.VIDEO,
        attr:{
            [learnAttributes.SUBJECT_ID]: videoData.subject_id,
            [learnAttributes.CHAPTER_ID]: videoData.chapter_id,
            [learnAttributes.TOPIC_ID]: videoData.topic_id,
            [learnAttributes.VIDEO_ID]: videoId,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:learnActionArea.SUBMIT_REPORT_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: learnScreenNavigation.SINGLE_VIDEO,
            [analyticsEndAttributes.TO_NODE]:  learnScreenNavigation.SINGLE_VIDEO,
          },
        feature:'video'
    }
    recordEvent(params)
}