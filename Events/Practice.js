import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType, analyticsEndAttributes} from '../Constants/Analytics'
import {getFilterAttributes} from 'analytics';
import {recordEvent} from '../trigger-event'

import {testAttributes, testScreenActionArea, testScreenNavigation, testNavigation} from '../Constants/Test'
import {practiceNavigation, practiceUserActionArea} from '../Constants/Practice'

//--------------------Topic List Active Mock Exam List--------------------------
export const RE_onExamCardClick=(exam, type)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: exam.test_id,
            [testAttributes.DELIVERY_ID]: exam.delivery_id,
            [testAttributes.EXAM_STATE]: exam.exam_state,
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: type,
            [testAttributes.TEST_NAME]: exam.exam_name,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:testScreenActionArea.ACTIVE_EXAM_ITEM,
            [analyticsEndAttributes.FROM_NODE]:testScreenNavigation.ACTIVE_TEST,
            [analyticsEndAttributes.TO_NODE]:testNavigation.TEST_ENGINE,
          },
        feature:'practice-tests'
    }
    recordEvent(params)
}

//--------------------Modal List Attempted Go To Result--------------------------
export const RE_onAttemptedCardClick=(test_id, delivery_id, category, test_name)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testAttributes.TEST_ID]: test_id,
            [testAttributes.DELIVERY_ID]: delivery_id,
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: category,
            [testAttributes.TEST_NAME]: test_name,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:testScreenActionArea.ATTEMPTED_EXAM_ITEM,
            [analyticsEndAttributes.FROM_NODE]:testScreenNavigation.ATTEMPTED_TEST,
            [analyticsEndAttributes.TO_NODE]:testScreenNavigation.RESULT,
          },
        feature:'practice-tests'
    }
    recordEvent(params)
}

//--------------------Modal List Header Tab Change--------------------------
export const RE_onHeaderTabsSelect=(testType,getFromRouteContainer, getToRouteContainer,e)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testScreenNavigation.EXAM_CATEGORY]: testType,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsEventGroup.TABS_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: testScreenActionArea.EXAM_TYPE_TAB,
            [analyticsEndAttributes.FROM_NODE]: getFromRouteContainer(),
            [analyticsEndAttributes.TO_NODE]: getToRouteContainer(e),
          },
        feature:'practice-tests'
    }
    recordEvent(params)
}

//--------------------Modal List Header Go Back--------------------------
export const RE_onHeaderGoBackClick=(testType, getFromRouteContainer)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testScreenNavigation.EXAM_CATEGORY]: testType,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsEventGroup.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:testScreenActionArea.BACK_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: getFromRouteContainer(),
            [analyticsEndAttributes.TO_NODE]: testScreenNavigation.MODEL_LIST,
          },
        feature:'practice-tests'
    }
    recordEvent(params)
}

//--------------------Test Modals Go To Test Screen--------------------------
export const RE_onTestModalClick=(testType)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testScreenNavigation.EXAM_CATEGORY]: testType,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:testScreenActionArea.MODEL_CARD,
            [analyticsEndAttributes.FROM_NODE]: testScreenNavigation.MODEL_LIST,
            [analyticsEndAttributes.TO_NODE]: testScreenNavigation.ACTIVE_TEST,
          },
        feature:'practice-tests'
    }
    recordEvent(params)
}

//--------------------Test Modals Go Back--------------------------
export const RE_onHeaderWrapGoBackClick=(testType)=>{
    const params={
        eventName:analyticsEventGroup.TEST,
        attr:{
            [testScreenNavigation.EXAM_CATEGORY]: testType,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsEventGroup.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:testScreenActionArea.BACK_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: testScreenNavigation.MODEL_LIST,
            [analyticsEndAttributes.TO_NODE]: 'practice',
          },
        feature:'practice-tests'
    }
    recordEvent(params)
}

//--------------------Practice Chapter Exercise--------------------------
export const RE_onPracticeChapterCardSelectByChapter=()=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: practiceUserActionArea.PRACTICE_TOPIC,
            [analyticsEndAttributes.FROM_NODE]: testScreenNavigation.PRACTICE_TOPIC,
            [analyticsEndAttributes.TO_NODE]: practiceNavigation.PRACTICE_CHAPTER_EXERCISE_LEVELS_DIALOG,
          },
        feature:'practice-tests'
    }
    recordEvent(params)
}


//--------------------Practice Topic Exercise--------------------------
export const RE_onPracticeChapterCardSelectByTopic=()=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: practiceUserActionArea.PRACTICE_TOPIC,
            [analyticsEndAttributes.FROM_NODE]: testScreenNavigation.PRACTICE_TOPIC,
            [analyticsEndAttributes.TO_NODE]: practiceNavigation.PRACTICE_TOPIC_EXERCISE_LEVELS_DIALOG,
          },
        feature:'practice-tests'
    }
    recordEvent(params)
}

//--------------------Topics Chapter View Progress--------------------------
export const RE_onPracticeChapterCardViewProgressSelect=()=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: practiceUserActionArea.PRACTICE_TOPIC,
            [analyticsEndAttributes.FROM_NODE]: testScreenNavigation.PRACTICE_TOPIC,
            [analyticsEndAttributes.TO_NODE]: practiceNavigation.PRACTICE_PROGRESS,
          },
        feature:'practice-tests'
    }
    recordEvent(params)
}

//--------------------Chapter Go To exercise--------------------------
export const RE_onLevelModalGoExerciseClick=(helpers)=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: practiceUserActionArea.PRACTICE_HOME,
            [analyticsEndAttributes.FROM_NODE]: testScreenNavigation.PRACTICE_TOPIC,
            [analyticsEndAttributes.TO_NODE]: helpers.type==="chapter"?practiceNavigation.PRACTICE_CHAPTER_EXERCISE:practiceNavigation.PRACTICE_TOPIC_EXERCISE,
          },
        feature:'practice-tests'
    }
    recordEvent(params)
}

//--------------------Go To Topic Screen--------------------------
export const RE_onSubjectCardGoToTopicScreenClick=()=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: practiceUserActionArea.PRACTICE_HOME,
            [analyticsEndAttributes.FROM_NODE]: testScreenNavigation.PRACTICE,
            [analyticsEndAttributes.TO_NODE]: practiceNavigation.PRACTICE_TOPIC,
        },
        feature:'practice-tests'
    }
    recordEvent(params)
}

//--------------------Go To Test Modal Screen--------------------------
export const RE_onExamCategoryGoToTestModalScreenClick=(type)=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            [testScreenNavigation.EXAM_CATEGORY]: type,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:testScreenActionArea.TAKE_TEST,
            [analyticsEndAttributes.FROM_NODE]:testScreenNavigation.PRACTICE,
            [analyticsEndAttributes.TO_NODE]:testScreenNavigation.MODEL_LIST,
          },
        feature:'practice-tests'
    }
    recordEvent(params)
}

//--------------------Go To Exercise--------------------------
export const RE_onExerciseCardClick=(helpers)=>{
    const params={
        eventName:analyticsEventGroup.PRACTICE,
        attr:{
            ...getFilterAttributes(
            analyticsEventType.USER_ACTION,
            analyticsUserActionKeyType.CLICK,
            analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: practiceUserActionArea.PRACTICE_HOME,
            [analyticsEndAttributes.FROM_NODE]: testScreenNavigation.PRACTICE,
            [analyticsEndAttributes.TO_NODE]: helpers.type==="chapter"?practiceNavigation.PRACTICE_CHAPTER_EXERCISE:practiceNavigation.PRACTICE_TOPIC_EXERCISE,
        },
        feature:'practice-tests'
    }
    recordEvent(params)
}