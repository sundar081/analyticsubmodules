import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType} from '../Constants/Analytics'
import {getFilterAttributes,getCommonAttributes} from 'analytics';
import {recordEvent} from '../trigger-event'

import {profileUserActionArea, profileNavigation} from '../Constants/Profile'

//--------------------Profile Changes Save--------------------------
export const RE_onSaveChangesButtonClick=()=>{
    const params={
        eventName:analyticsEventGroup.PROFILE,
        attr:{
            ...getCommonAttributes(
              profileUserActionArea.SAVE_CHANGES,
              profileNavigation.PERSONAL_INFO,
              profileNavigation.PERSONAL_INFO,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
          },
        feature:'profile'
    }
    recordEvent(params)
}
