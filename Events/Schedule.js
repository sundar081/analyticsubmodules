import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType, analyticsEndAttributes} from '../Constants/Analytics'
import {getFilterAttributes} from 'analytics';
import {recordEvent} from '../trigger-event'

import {scheduleAttributes, scheduleUserActionArea, scheduleNavigation} from '../Constants/Schedule'

//--------------------Schedule Hanlde Month Change-------------------------
export const RE_onSubjectDropDownClick=(e)=>{
    const params={
        eventName:analyticsEventGroup.SCHEDULE,
        attr:{
            [scheduleAttributes.MONTH_NAME]: e,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: scheduleUserActionArea.MONTH_DROPDOWN,
            [analyticsEndAttributes.FROM_NODE]: scheduleNavigation.SCHEDULE,
            [analyticsEndAttributes.TO_NODE]:  scheduleNavigation.SCHEDULE,
          },
        feature:'schedule'
    }
    recordEvent(params)
}

//--------------------Schedule Hanlde Zoom-------------------------
export const RE_onZoomBarButtonClick=()=>{
    const params={
        eventName:analyticsEventGroup.SCHEDULE,
        attr:{
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: scheduleUserActionArea.ZOOM_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: scheduleNavigation.SCHEDULE,
            [analyticsEndAttributes.TO_NODE]:  scheduleNavigation.SCHEDULE,
          },
        feature:'schedule'
    }
    recordEvent(params)
}

//--------------------Schedule Go To Next Month-------------------------
export const RE_onNavNextButtonClick=(monthsList, mnthIndex)=>{
    const params={
        eventName:analyticsEventGroup.SCHEDULE,
        attr:{
            [scheduleAttributes.MONTH_NAME]: monthsList[mnthIndex],
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:
            scheduleUserActionArea.NEXT_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: scheduleNavigation.SCHEDULE,
            [analyticsEndAttributes.TO_NODE]:  scheduleNavigation.SCHEDULE,
          },
        feature:'schedule'
    }
    recordEvent(params)
}

//--------------------Schedule Go To Previous Month-------------------------
export const RE_onNavPrevButtonClick=(monthsList, mnthIndex)=>{
    const params={
        eventName:analyticsEventGroup.SCHEDULE,
        attr:{
            [scheduleAttributes.MONTH_NAME]: monthsList[mnthIndex],
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:
            scheduleUserActionArea.PREVIOUS_BUTTON,
            [analyticsEndAttributes.FROM_NODE]: scheduleNavigation.SCHEDULE,
            [analyticsEndAttributes.TO_NODE]:  scheduleNavigation.SCHEDULE,
          },
        feature:'schedule'
    }
    recordEvent(params)
}