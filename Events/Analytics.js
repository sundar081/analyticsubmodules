import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType, analyticsEndAttributes, analyticsFeatureUserActionArea, analyticsFeatureNavigation, analyticsFeatureAttributes} from '../Constants/Analytics'
import {getFilterAttributes,} from 'analytics';
import { testAttributes, testNavigation } from '../Constants/Test';
import {recordEvent} from '../trigger-event'


//--------------------View Result---------------------------
export const RE_onTestListCardClick=(test_id, delivery_id, category)=>{
    const params={
        eventName:analyticsEventGroup.ANALYTICS_FEATURE,
        attr:{
            [testAttributes.TEST_ID]: test_id,
            [testAttributes.DELIVERY_ID]: delivery_id,
            [testAttributes.TEST_URI_ID]: '',
            [testAttributes.EXAM_CATEGORY]: category,
            [testAttributes.TEST_NAME]: 'scheduled_test',
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:analyticsFeatureUserActionArea.VIEW_RESULT,
            [analyticsEndAttributes.FROM_NODE]: analyticsFeatureNavigation.PERFORMANCE,
            [analyticsEndAttributes.TO_NODE]: testNavigation.TEST_ENGINE_RESULT,
          },
        feature:'analytics'
    }
    recordEvent(params)
}

//--------------------Select Score Subject---------------------------
export const RE_onScoreAnalysisLevelDropClick=(selectedSubjected)=>{
    const params={
        eventName:analyticsEventGroup.ANALYTICS_FEATURE,
        attr:{
            [analyticsFeatureAttributes.SUBJECT_ID]: selectedSubjected.id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:analyticsFeatureUserActionArea.SUBJECT_DROPDOWN,
            [analyticsEndAttributes.FROM_NODE]: analyticsFeatureNavigation.SCORE_GRAPH,
            [analyticsEndAttributes.TO_NODE]: analyticsFeatureNavigation.SCORE_GRAPH,
          },
        feature:'analytics'
    }
    recordEvent(params)
}
//--------------------Select Rank Subject---------------------------
export const RE_onRankAnalysisLevelDropClick=(selectedSubjected)=>{
    const params={
        eventName:analyticsEventGroup.ANALYTICS_FEATURE,
        attr:{
            [analyticsFeatureAttributes.SUBJECT_ID]: selectedSubjected.id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:analyticsFeatureUserActionArea.SUBJECT_DROPDOWN,
            [analyticsEndAttributes.FROM_NODE]: analyticsFeatureNavigation.RANK_GRAPH,
            [analyticsEndAttributes.TO_NODE]: analyticsFeatureNavigation.RANK_GRAPH,
          },
        feature:'analytics'
    }
    recordEvent(params)
}

//--------------------Select Question Level---------------------------
export const RE_onFilteredSubjectListByquestionLevelClick=(selectedlevel)=>{
    const params={
        eventName:analyticsEventGroup.ANALYTICS_FEATURE,
        attr:{
            [analyticsFeatureAttributes.LEVEL_ID]: selectedlevel.id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:analyticsFeatureUserActionArea.LEVEL_DROPDOWN,
            [analyticsEndAttributes.FROM_NODE]: analyticsFeatureNavigation.QUESTION_ATTEMPTED_GRAPH,
            [analyticsEndAttributes.TO_NODE]: analyticsFeatureNavigation.QUESTION_ATTEMPTED_GRAPH,
          },
        feature:'analytics'
    }
    recordEvent(params)
}

//--------------------Select Time Level---------------------------
export const RE_onFilteredTimeListByquestionLevelClick=(selectedlevel)=>{
    const params={
        eventName:analyticsEventGroup.ANALYTICS_FEATURE,
        attr:{
            [analyticsFeatureAttributes.LEVEL_ID]: selectedlevel.id,
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]:analyticsFeatureUserActionArea.LEVEL_DROPDOWN,
            [analyticsEndAttributes.FROM_NODE]: analyticsFeatureNavigation.AVG_TIME_PER_QUESTION_GRAPH,
            [analyticsEndAttributes.TO_NODE]: analyticsFeatureNavigation.AVG_TIME_PER_QUESTION_GRAPH,
          },
        feature:'analytics'
    }
    recordEvent(params)
}

//--------------------Tab Change---------------------------
export const RE_onHeaderTabsClickTochangeTab=(routeContainer, routeContainerKey,key)=>{
    const params={
        eventName:analyticsEventGroup.ANALYTICS_FEATURE,
        attr:{
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsEventGroup.TABS_NAVIGATION,
            ),
            [analyticsEndAttributes.USER_ACTION_AREA]: analyticsFeatureUserActionArea.ANALYTICS_TYPE_TAB,
            [analyticsEndAttributes.FROM_NODE]: routeContainer(),
            [analyticsEndAttributes.TO_NODE]: routeContainerKey(key),
          },
        feature:'analytics'
    }
    recordEvent(params)
}