import { analyticsEventGroup, analyticsEventType } from '../Constants/AnalyticEvent'
import {analyticsNavigationType, analyticsUserActionKeyType} from '../Constants/Analytics'
import {assignmentsUserActionArea, assignmentsNavigation, assignmentsAttributes} from '../Constants/Assignments'
import {getCommonAttributes,getFilterAttributes,} from 'analytics';
import {recordEvent} from '../trigger-event'

//--------------------Assignment Tab Change---------------------------
export const RE_onHeaderTabsSelect=(tabkey, key)=>{
    const params={
        eventName:analyticsEventGroup.ASSIGNMENTS,
        attr:{
            ...getCommonAttributes(
              assignmentsUserActionArea.TAB_CHANGE,
              tabkey === 'questions' ? assignmentsNavigation.ASSIGNMENT_QUESTIONS : tabkey === 'submission' ? assignmentsNavigation.ASSIGNMENT_SUBMISSION : assignmentsNavigation.ASSIGNMENT_SOLUTIONS,
              key === 'questions' ? assignmentsNavigation.ASSIGNMENT_QUESTIONS : key === 'submission' ? assignmentsNavigation.ASSIGNMENT_SUBMISSION : assignmentsNavigation.ASSIGNMENT_SOLUTIONS,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.IN_SCREEN_NAVIGATION,
            ),
          },
        feature:'assignments'
    }
    recordEvent(params)
}

//--------------------Assignment Card Details---------------------------
export const RE_onStyleMediumCardClick=(data)=>{
    const params={
        eventName:analyticsEventGroup.ASSIGNMENTS,
        attr:{
            [assignmentsAttributes.ASSIGNMENT_DATE]:data.assignment_date,
            [assignmentsAttributes.ASSIGNMENT_ID]:data.assignment_id,
            [assignmentsAttributes.ASSIGNMENT_NAME]:data.assignment_name,
            [assignmentsAttributes.ASSIGNMENT_STATUS]:data.assignment_status,
            [assignmentsAttributes.CHAPTER_NAME]:data.chapter_name,
            [assignmentsAttributes.DELIVERY_ID]:data.delivery_id,
            [assignmentsAttributes.DURATION]:data.duration,
            [assignmentsAttributes.SUBJECT_NAME]:data.subject_name,
            [assignmentsAttributes.SUBMISSION_DEADLINE]:data.submission_deadline,
            ...getCommonAttributes(
              assignmentsUserActionArea.ASSIGNMENT_CARD,
              assignmentsUserActionArea.ASSIGNMENT_CARDS,
              assignmentsUserActionArea.ASSIGNMENT_QUESTIONS,
              'success',
            ),
            ...getFilterAttributes(
              analyticsEventType.USER_ACTION,
              analyticsUserActionKeyType.CLICK,
              analyticsNavigationType.NAVIGATION,
            ),
          },
        feature:'assignments'
    }
    recordEvent(params)
}
